#include "Application.h"
#include "ApplicationCfg.h"
#include <FxsAssert.hpp>
#include <vector>

Application gApplication;

static std::vector<UpdateHandler*> gUpdateHandlers;

//-----------------------------------------------------------------------------
Application::Application()
	:
	window(nullptr)
{
}
//-----------------------------------------------------------------------------
Application::~Application()
{
}
//-----------------------------------------------------------------------------
void Application::Init()
{
	appCfg.Init();
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	window = glfwCreateWindow(appCfg.window.width, appCfg.window.height,
		appCfg.window.title.c_str(), NULL, NULL);
	glfwMakeContextCurrent(window);

	glewExperimental = GL_TRUE;
	glewInit();
	glGetError();
	FXS_ASSERT(window);
}
//-----------------------------------------------------------------------------
void Application::Deinit()
{
	glfwTerminate();
	appCfg.Deinit();
}
//-----------------------------------------------------------------------------
void Application::Run()
{
	double t = glfwGetTime();
	double dt;

	while (!glfwWindowShouldClose(window)) {
		dt = glfwGetTime() - t;
		t = glfwGetTime();

		for (UpdateHandler* handler : gUpdateHandlers) {
			handler->OnUpdate(dt);
		}

		glfwSwapBuffers(window);
		glfwPollEvents();
	}
}
//-----------------------------------------------------------------------------
void Application::AddUpdateHandler(UpdateHandler& handler)
{
	gUpdateHandlers.push_back(&handler);
}
//-----------------------------------------------------------------------------