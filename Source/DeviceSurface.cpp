#include "DeviceSurface.hpp"

//-----------------------------------------------------------------------------
DeviceSurface::DeviceSurface(const Mesh& mesh)
	:
	mMaterial(*mesh.material)
{
	GLsizei size = mesh.positions.size() * sizeof(glm::vec3);
	mPositions.Init(GL_ARRAY_BUFFER, size, mesh.positions.data(), GL_STATIC_DRAW);
	mNormals.Init(GL_ARRAY_BUFFER, size, mesh.normals.data(), GL_STATIC_DRAW);
	mVertexArray.Init(GL_TRIANGLES, mesh.GetVertexCount());
	mVertexArray.Bind();
	mVertexArray.SetAttribute(mPositions, 0, 3);
	mVertexArray.SetAttribute(mNormals, 1, 3);

	FXS_GL_ASSERT_SUCCESS();
}
//-----------------------------------------------------------------------------
DeviceSurface::~DeviceSurface()
{
	mPositions.Deinit();
	mNormals.Deinit();
	mVertexArray.Deinit();
}
//-----------------------------------------------------------------------------
void DeviceSurface::Draw() const
{
	mVertexArray.Bind();
	mVertexArray.DrawArrays();
}
//-----------------------------------------------------------------------------