#ifndef GBUFFER_HPP
#define GBUFFER_HPP

#include <FxsClassDecls.hpp>
#include <FxsGLFramebuffer.hpp>
#include <FxsGLProgram.hpp>

class GBuffer {
	FXS_DECL_SINGLETON(GBuffer)
public:
	void Init();
	void Deinit();
	void Update();

	FXS_RO_MEMBER(glm::vec2, Resolution);
	FXS_RO_MEMBER(fxs::GLTexture2D, Depths);
	FXS_RO_MEMBER(fxs::GLTexture2D, LinearDepths);
	FXS_RO_MEMBER(fxs::GLTexture2D, Normals);
	FXS_RO_MEMBER(fxs::GLTexture2D, Albedos);
	FXS_RO_MEMBER(fxs::GLTexture2D, Reflectivities);

private:
	fxs::GLProgram mProgram;
	fxs::GLFramebuffer mFramebuffer;
};



#endif