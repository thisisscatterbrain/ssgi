#ifndef APPLICATION_H
#define APPLICATION_H

#include "UpdateHandler.h"
#include <FxsGLCommon.hpp>

class Application {
public:
	Application();
	~Application();

	void Init();
	void Deinit();
	void AddUpdateHandler(UpdateHandler& updateHandler);
	void Run();

	GLFWwindow* window;
};

extern Application gApplication;

#endif