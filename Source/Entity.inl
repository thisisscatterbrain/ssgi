//-----------------------------------------------------------------------------
template <class ENTITY, size_t SIZE>
EntityPool<ENTITY, SIZE>::EntityPool()
{
	uintptr_t alignment = __alignof(ENTITY);
	uintptr_t address = reinterpret_cast<uintptr_t>(buffer);
	uintptr_t alignedAddress = (address + alignment) & (~(alignment - 1));
	alignedBuffer = reinterpret_cast<uint8_t*>(alignedAddress);
}
//-----------------------------------------------------------------------------
template <class ENTITY, size_t SIZE>
void* EntityPool<ENTITY, SIZE>::Allocate()
{
	count++;

	if ((count + 1)* sizeof(ENTITY) > SIZE) {
		FXS_LOG("Out of memory.", "");
		FXS_ASSERT(false);
	}

	return alignedBuffer + (count - 1)* sizeof(ENTITY);
}
//-----------------------------------------------------------------------------
template <class ENTITY, size_t SIZE>
void EntityPool<ENTITY, SIZE>::Reset()
{
	for (size_t i = 0; i < count; i++) {
		ENTITY* e = reinterpret_cast<ENTITY*>(alignedBuffer)+i;
		e->~ENTITY();
	}

	count = 0;
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
template <class ENTITY, size_t SIZE>
ENTITY* EntityPool<ENTITY, SIZE>::begin()
{
	return reinterpret_cast<ENTITY*>(alignedBuffer);
}
//-----------------------------------------------------------------------------
template <class ENTITY, size_t SIZE>
ENTITY* EntityPool<ENTITY, SIZE>::end()
{
	return reinterpret_cast<ENTITY*>(alignedBuffer) + count;
}
//-----------------------------------------------------------------------------
template <class ENTITY, size_t SIZE>
size_t EntityPool<ENTITY, SIZE>::GetCount()
{
	return count;
}
//-----------------------------------------------------------------------------
template <class ENTITY, size_t SIZE>
void* Entity<ENTITY, SIZE>::operator new(size_t size)
{
	return pool.Allocate();
}
//-----------------------------------------------------------------------------
template <class ENTITY, size_t SIZE>
void Entity<ENTITY, SIZE>::operator delete (void* ptr)
{
	FXS_LOG("Warning: Entities cannot be deleted seperately", "");
}
//-----------------------------------------------------------------------------
