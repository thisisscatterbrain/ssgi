#ifndef FULLSCREENQUAD_HPP
#define FULLSCREENQUAD_HPP

#include <FxsGLVertexArray.hpp>

class FullscreenQuad {
public:
	void Init();
	void Deinit();
	void Draw();

private:
	static fxs::GLVertexArray msVertexArray;
	static fxs::GLBuffer msBuffer;
	static int msReferenceCount;
};

#endif