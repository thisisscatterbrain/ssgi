#ifndef SCENE_H
#define SCENE_H

#include <string>

struct Scene {
	// Loads all scene components, i.e. camera, meshes and materials and lights.
	void Load(const std::string& filename);
};

extern Scene gScene;

#endif