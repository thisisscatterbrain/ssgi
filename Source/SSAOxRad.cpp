#include "SSAOxRad.hpp"
#include "RenderCfg.h"
#include "Camera.h"
#include "GBuffer.hpp"
#include "RadiosityBuffer.hpp"

SSAOxRad SSAOxRad::instance;

//-----------------------------------------------------------------------------
SSAOxRad::SSAOxRad()
{
}
//-----------------------------------------------------------------------------
SSAOxRad::~SSAOxRad()
{
}
//-----------------------------------------------------------------------------
void SSAOxRad::Init()
{
	//-------------------------------------------------------------------------
	// SSGI.
	//-------------------------------------------------------------------------
	mProgram.InitWithFiles({"../Shader/Fullscreen.vs", "../Shader/SSAOxRad.fs", 
		"../Shader/PRNG.fs"});
	mProgram.Use();
	mProgram["gBuffer.resolution"] = RenderCfg::instance.gBuffer.resolution;
	mProgram["gBuffer.linearDepths"] = 0;
	mProgram["gBuffer.normals"] = 1;
	mProgram["gBuffer.albedos"] = 2;
	mProgram["gBuffer.reflectivities"] = 3;
	mProgram["radiosityBuffer.radiosities"] = 4;

#ifndef TWEAKING_ENABLED
	mProgram["ssgiCfg.sampleCount"] = RenderCfg::instance.ssgi.sampleCount;
	mProgram["ssgiCfg.radius"] = RenderCfg::instance.ssgi.radius;
	mProgram["ssgiCfg.aaoBeta"] = RenderCfg::instance.ssgi.aaoBeta;
	mProgram["ssgiCfg.aaoEps"] = RenderCfg::instance.ssgi.aaoEps;
	mProgram["ssgiCfg.aaoSigma"] = RenderCfg::instance.ssgi.aaoSigma;
	mProgram["ssgiCfg.radAs"] = RenderCfg::instance.ssgi.radAs;
	mProgram["ssgiCfg.ssrRadius"] = RenderCfg::instance.ssgi.ssrRadius;
#endif

	mIndirectDiffuse.Init(RenderCfg::instance.gBuffer.resolution, 
		GL_R11F_G11F_B10F, GL_LINEAR, GL_CLAMP_TO_BORDER);
	mIndirectSpecular.Init(RenderCfg::instance.gBuffer.resolution,
		GL_R11F_G11F_B10F, GL_LINEAR, GL_CLAMP_TO_BORDER);
	mGlossyness.Init(RenderCfg::instance.gBuffer.resolution, 
		GL_R32F, GL_LINEAR, GL_CLAMP_TO_BORDER);

	mFramebuffer.Init(RenderCfg::instance.gBuffer.resolution);
	mFramebuffer.Bind();
	mFramebuffer.Attach(GL_COLOR_ATTACHMENT0, mIndirectDiffuse);
	mFramebuffer.Attach(GL_COLOR_ATTACHMENT1, mIndirectSpecular);
	mFramebuffer.Attach(GL_COLOR_ATTACHMENT2, mGlossyness);
	GLenum drawBufs[] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, 
		GL_COLOR_ATTACHMENT2 };
	mFramebuffer.SetDrawBuffers(3, drawBufs);

	//-------------------------------------------------------------------------
	// Blur.
	//-------------------------------------------------------------------------
	mProgramBlur.InitWithFiles({"../Shader/Fullscreen.vs", "../Shader/BlurSSGI.fs"});
	mProgramBlur.Use();
	mProgramBlur["gBuffer.linearDepths"] = 0;
	mProgramBlur["gBuffer.normals"] = 1;
	mProgramBlur["ssgi.resolution"] = RenderCfg::instance.gBuffer.resolution;
	mProgramBlur["ssgi.indirectDiffuse"] = 2;

#ifndef TWEAKING_ENABLED
	mProgramBlur["blur.radius"] = float(RenderCfg::instance.ssgi.blurRadius);
	mProgramBlur["blur.sigmaS"] = RenderCfg::instance.ssgi.blurSigmaS;
	mProgramBlur["blur.sigmaR"] = RenderCfg::instance.ssgi.blurSigmaR;
	mProgramBlur["blur.sigmaR2"] = RenderCfg::instance.ssgi.blurSigmaR2;
#endif

	mIndirectDiffuseTemp.Init(RenderCfg::instance.gBuffer.resolution,
		GL_R11F_G11F_B10F, GL_LINEAR, GL_CLAMP_TO_BORDER);
	mFramebufferBlurTemp.Init(RenderCfg::instance.gBuffer.resolution);
	mFramebufferBlurTemp.Bind();
	mFramebufferBlurTemp.Attach(GL_COLOR_ATTACHMENT0, mIndirectDiffuseTemp);
	mFramebufferBlurTemp.SetDrawBuffers(1, drawBufs);

	mFramebufferBlur.Init(RenderCfg::instance.gBuffer.resolution);
	mFramebufferBlur.Bind();
	mFramebufferBlur.Attach(GL_COLOR_ATTACHMENT0, mIndirectDiffuse);
	mFramebufferBlur.SetDrawBuffers(1, drawBufs);

	//-------------------------------------------------------------------------
	// Specular Blur.
	//-------------------------------------------------------------------------
	mProgramBlurSpec.InitWithFiles({"../Shader/Fullscreen.vs", "../Shader/BlurSSR.fs"});
	mProgramBlurSpec.Use();
	mProgramBlurSpec["resolution"] = RenderCfg::instance.gBuffer.resolution;
	mProgramBlurSpec["reflectivity"] = 0;
	mProgramBlurSpec["indirectSpecular"] = 1;
	mProgramBlurSpec["glossyness"] = 2;

#ifndef TWEAKING_ENABLED
	mProgramBlurSpec["blur.radius"] = float(RenderCfg::instance.ssgi.ssrBlurRadius);
	mProgramBlurSpec["blur.sigmaS"] = RenderCfg::instance.ssgi.ssrBlurSigmaS;
#endif

	mIndirectSpecularTemp.Init(RenderCfg::instance.gBuffer.resolution,
		GL_R11F_G11F_B10F, GL_LINEAR, GL_CLAMP_TO_BORDER);
	mFramebufferBlurSpecTemp.Init(RenderCfg::instance.gBuffer.resolution);
	mFramebufferBlurSpecTemp.Bind();
	mFramebufferBlurSpecTemp.Attach(GL_COLOR_ATTACHMENT0, mIndirectSpecularTemp);
	mFramebufferBlurSpecTemp.SetDrawBuffers(1, drawBufs);

	mFramebufferBlurSpec.Init(RenderCfg::instance.gBuffer.resolution);
	mFramebufferBlurSpec.Bind();
	mFramebufferBlurSpec.Attach(GL_COLOR_ATTACHMENT0, mIndirectSpecular);
	mFramebufferBlurSpec.SetDrawBuffers(1, drawBufs);

	//-------------------------------------------------------------------------
	// SSR
	//-------------------------------------------------------------------------
	mProgramRefl.InitWithFiles({"../Shader/Fullscreen.vs", "../Shader/SSR.fs"});
	mProgramRefl.Use();

	mProgramRefl["gBuffer.resolution"] = RenderCfg::instance.gBuffer.resolution;
	mProgramRefl["gBuffer.linearDepths"] = 0;
	mProgramRefl["gBuffer.normals"] = 1;
	mProgramRefl["gBuffer.albedos"] = 2;
	mProgramRefl["gBuffer.reflectivities"] = 3;
	mProgramRefl["lightFields.radiosities"] = 4;
	mProgramRefl["lightFields.indirectDiffuse"] = 5;

#ifndef TWEAKING_ENABLED
	mProgramRefl["ssgiCfg.ssrRadius"] = RenderCfg::instance.ssgi.ssrRadius;
#endif

	mReflections.Init(RenderCfg::instance.gBuffer.resolution, 
		GL_R11F_G11F_B10F, GL_LINEAR, GL_CLAMP_TO_BORDER);
	mFramebufferRefl.Init(RenderCfg::instance.gBuffer.resolution);
	mFramebufferRefl.Bind();
	mFramebufferRefl.Attach(GL_COLOR_ATTACHMENT0, mReflections);
	mFramebufferRefl.SetDrawBuffers(1, drawBufs);

	mFullscreenQuad.Init();
}
//-----------------------------------------------------------------------------
void SSAOxRad::Deinit()
{
	mProgram.Deinit();
	mIndirectDiffuse.Deinit();
	mIndirectSpecular.Deinit();
	mGlossyness.Deinit();
	mFramebuffer.Deinit();

	mProgramBlur.Deinit();
	mIndirectDiffuseTemp.Deinit();
	mFramebufferBlur.Deinit();

	mProgramBlurSpec.Deinit();
	mFramebufferBlurSpecTemp.Deinit();
	mFramebufferBlurSpec.Deinit();

	mProgramRefl.Deinit();
	mReflections.Deinit();
	mFramebufferRefl.Deinit();

	mFullscreenQuad.Deinit();
}
//-----------------------------------------------------------------------------
void SSAOxRad::Update()
{
	glDisable(GL_DEPTH_TEST);
	mFramebuffer.Bind();
	mFramebuffer.Clear();
	mProgram.Use();
	mProgram["camera.tanFovy2"] = Camera.GetTanFovy2();
	mProgram["camera.aspect"] = Camera.GetAspect();
	mProgram["camera.viewInv"] = Camera.GetViewInv();
	mProgram["camera.viewNormal"] = Camera.GetViewNormal();
	mProgram["camera.perspective"] = Camera.GetPerspective();

#ifdef TWEAKING_ENABLED
	mProgram["ssgiCfg.sampleCount"] = RenderCfg::instance.ssgi.sampleCount;
	mProgram["ssgiCfg.radius"] = RenderCfg::instance.ssgi.radius;
	mProgram["ssgiCfg.aaoBeta"] = RenderCfg::instance.ssgi.aaoBeta;
	mProgram["ssgiCfg.aaoEps"] = RenderCfg::instance.ssgi.aaoEps;
	mProgram["ssgiCfg.aaoSigma"] = RenderCfg::instance.ssgi.aaoSigma;
	mProgram["ssgiCfg.radAs"] = RenderCfg::instance.ssgi.radAs;
	mProgram["ssgiCfg.ssrRadius"] = RenderCfg::instance.ssgi.ssrRadius;
#endif

	GBuffer::instance.GetLinearDepths().Bind(0);
	GBuffer::instance.GetNormals().Bind(1);
	GBuffer::instance.GetAlbedos().Bind(2);
	GBuffer::instance.GetReflectivities().Bind(3);
	RadiosityBuffer::instance.GetRadiosities().Bind(4);
	mFullscreenQuad.Draw();

	// Blur.
	GBuffer::instance.GetLinearDepths().Bind(0);
	GBuffer::instance.GetNormals().Bind(1);
	mProgramBlur.Use();

#ifdef TWEAKING_ENABLED
	mProgramBlur["blur.radius"] = float(RenderCfg::instance.ssgi.blurRadius);
	mProgramBlur["blur.sigmaS"] = RenderCfg::instance.ssgi.blurSigmaS;
	mProgramBlur["blur.sigmaR"] = RenderCfg::instance.ssgi.blurSigmaR;
	mProgramBlur["blur.sigmaR2"] = RenderCfg::instance.ssgi.blurSigmaR2;
#endif

	mFramebufferBlurTemp.Bind();
	mFramebufferBlurTemp.Clear();
	mProgramBlur["blur.direction"] = glm::vec2(0.0, 1.0);
	mIndirectDiffuse.Bind(2);
	mFullscreenQuad.Draw();

	mFramebufferBlur.Bind();
	mProgramBlur["blur.direction"] = glm::vec2(1.0, 0.0);
	mIndirectDiffuseTemp.Bind(2);
	mFullscreenQuad.Draw();

	//-------------------------------------------------------------------------
	// Specular Blur.
	//-------------------------------------------------------------------------

	mProgramBlurSpec.Use();

#ifdef TWEAKING_ENABLED
	mProgramBlurSpec["blur.radius"] = float(RenderCfg::instance.ssgi.ssrBlurRadius);
	mProgramBlurSpec["blur.sigmaS"] = RenderCfg::instance.ssgi.ssrBlurSigmaS;
#endif

	GBuffer::instance.GetReflectivities().Bind(0);
	mGlossyness.Bind(2);

	mFramebufferBlurSpecTemp.Bind();
	mFramebufferBlurSpecTemp.Clear(); // TODO: Probably this clear call can be avoided.
	mIndirectSpecular.Bind(1);
	mProgramBlurSpec["blur.direction"] = glm::vec2(1.0, 0.0);
	mFullscreenQuad.Draw();

	mFramebufferBlurSpec.Bind();
	mFramebufferBlurSpec.Clear(); // TODO: Probably this clear call can be avoided.
	mIndirectSpecularTemp.Bind(1);
	mProgramBlurSpec["blur.direction"] = glm::vec2(0.0, 1.0);
	mFullscreenQuad.Draw();

	//-------------------------------------------------------------------------
	// SSR
	//-------------------------------------------------------------------------
	mProgramRefl.Use();
	mProgramRefl["camera.tanFovy2"] = Camera.GetTanFovy2();
	mProgramRefl["camera.aspect"] = Camera.GetAspect();
	mProgramRefl["camera.viewNormal"] = Camera.GetViewNormal();
	mProgramRefl["camera.perspective"] = Camera.GetPerspective();

#ifdef TWEAKING_ENABLED
	mProgramRefl["ssgiCfg.ssrRadius"] = RenderCfg::instance.ssgi.ssrRadius;
	//mProgramRefl["ssgiCfg.ssrDistMin"] = RenderCfg::instance.ssgi.ssrDistMin;
	//mProgramRefl["ssgiCfg.ssrDistMax"] = RenderCfg::instance.ssgi.ssrDistMax;
#endif


	GBuffer::instance.GetLinearDepths().Bind(0);
	GBuffer::instance.GetNormals().Bind(1);
	GBuffer::instance.GetAlbedos().Bind(2);
	GBuffer::instance.GetReflectivities().Bind(3);
	RadiosityBuffer::instance.GetRadiosities().Bind(4);
	mIndirectDiffuse.Bind(5);

	mFramebufferRefl.Bind();
	mFramebufferRefl.Clear();

	mFullscreenQuad.Draw();
}
//-----------------------------------------------------------------------------