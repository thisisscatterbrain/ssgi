#ifndef CAMERA_H
#define CAMERA_H

#include <FxsGLCommon.hpp>

class Camera {
public:
	Camera();
	~Camera();

	// Setters.
	void SetNear(float near);
	void SetFar(float far);
	void SetFovy(float fovy); // [fovy] is in degrees!.
	void SetAspect(float asp);
	void SetPosition(const glm::vec3& position);
	void SetCenter(const glm::vec3& center);
	void SetUp(const glm::vec3& up);

	// Getters.
	const glm::mat4& GetPerspective() const;
	const glm::mat4& GetPerspectiveInv() const;
	const glm::mat4& GetView() const;
	const glm::mat3& GetViewNormal() const;
	const glm::mat4& GetViewInv() const;
	const glm::mat4& GetPerspectiveView() const;
	const glm::mat4& GetPerspectiveViewInv() const;
	const glm::vec3& GetPosition() const { return mPosition; }
	float GetNear() const { return mNear; }
	float GetFar() const { return mFar; }
	float GetAspect() const { return mAsp; }
	float GetTanFovy2() const;

	// Gets the points in world space making up the current near and far plane of 
	// the camera frustum. The first four points represent the near plane the last
	// four points represent the far plane.
	void GetFrustumPointsWC(glm::vec3 points[8]) const;

	const glm::vec3* GetFrustumDirections() const;

	// Camera motion

	// Translates the camera according to [translation].
	void Translate(const glm::vec3& translation);

	// Moves the camera along its u axis.
	void MoveU(float x);

	// Moves the camera along its n axis.
	void MoveN(float x);

	// Rotates around the y axis.
	void RotateY(float angle);

private:
	inline void Update() const;

	// Actual camera parameters.
	glm::vec3 mPosition;
	glm::vec3 mCenter;
	glm::vec3 mUp;

	float mNear;
	float mFar;
	float mFovy;
	float mAsp;

	// Indicates if the camera matrices need to be recomputed.
	mutable bool mIsDirty;

	// Information inferred from the camera parameters, stored for performance 
	// reasons.
	mutable glm::mat4 mPerspective;
	mutable glm::mat4 mPerspectiveInv;
	mutable glm::mat4 mView;
	mutable glm::mat4 mViewInv;
	mutable glm::mat3 mViewNormal;
	mutable glm::mat4 mPerspectiveView;
	mutable glm::mat4 mPerspectiveViewInv;
	mutable float mTanFovy2;

	// Basis vectors of the view space.
	mutable glm::vec3 mXV;
	mutable glm::vec3 mYV;
	mutable glm::vec3 mZV;

	// Four directions used for reconstructing world space position from depth.
	mutable glm::vec3 mFrustumDirections[4];
};

// The globally accessable scene camera.
extern class Camera Camera; 

#endif