#ifndef RADIOSITYBUFFER_HPP
#define RADIOSITYBUFFER_HPP

#include <FxsClassDecls.hpp>
#include <FxsGLFramebuffer.hpp>
#include <FxsGLProgram.hpp>
#include "FullscreenQuad.hpp"

class RadiosityBuffer {
	FXS_DECL_SINGLETON(RadiosityBuffer);
public:
	void Init();
	void Deinit();
	void Update();

	FXS_RO_MEMBER(fxs::GLTexture2D, Radiosities);

private:
	void SetLights();

	fxs::GLProgram mProgram;
	fxs::GLFramebuffer mFramebuffer;
	FullscreenQuad mFullscreenQuad;
};

#endif