#ifndef RENDERCFG_H
#define RENDERCFG_H

#include <FxsGLCommon.hpp>
#include <FxsClassDecls.hpp>

struct RenderCfg {
	FXS_DECL_SINGLETON(RenderCfg);
public:
	void Init();
	void Deinit();

	struct {
		glm::uvec2 resolution;
	} gBuffer;

	struct {
		GLfloat radius;	// Radius of influence in the real world.
		GLint sampleCount;
		GLint logPatternCount;

		GLfloat aaoSigma;
		GLfloat aaoK;
		GLfloat aaoBeta;
		GLfloat aaoEps;

		GLfloat radAs;

		GLfloat ssrRadius;
		GLfloat ssrDistMin;
		GLfloat ssrDistMax;
		GLfloat ssrBlurSigmaS;
		GLfloat ssrBlurRadius;

		GLint blurRadius;
		GLfloat blurSigmaS;
		GLfloat blurSigmaR;
		GLfloat blurSigmaR2;
	} ssgi;

	struct {
		float diffuse;
		float diffuseIndirect;
		float specularIndirect;
	} share;

	struct {
		GLfloat radius;
		GLfloat sigmaS;
		GLfloat sigmaR;

	} blur;

};


#endif