#ifndef DEVICESURFACE_HPP
#define DEVICESURFACE_HPP

#include <FxsClassDecls.hpp>
#include <FxsGLVertexArray.hpp>
#include "Mesh.h"

struct DeviceSurface : public Entity<DeviceSurface, 1024> {
public:
	DeviceSurface(const Mesh& mesh);
	~DeviceSurface();
	
	void Draw() const;

	FXS_RO_MEMBER(Material, Material);

private:
	fxs::GLVertexArray mVertexArray;
	fxs::GLBuffer mPositions;
	fxs::GLBuffer mNormals;	
};

#endif