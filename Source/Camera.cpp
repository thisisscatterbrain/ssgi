#include "Camera.h"

class Camera Camera;

//-----------------------------------------------------------------------------
Camera::Camera() 
	:
	mPosition(0.0f, 0.0f, 0.0f),
	mCenter(0.0f, 0.0f, -1.0f),
	mUp(0.0f, 1.0f, 0.0f),
	mNear(0.01f),
	mFar(100.0f),
	mFovy(glm::radians(60.0f)),
	mAsp(1280.0f / 720.0f),
	mIsDirty(true)
{
}
//-----------------------------------------------------------------------------
Camera::~Camera() 
{
}
//-----------------------------------------------------------------------------
void Camera::SetNear(float near)
{
	mIsDirty = true;
	mNear = near;
}
//-----------------------------------------------------------------------------
void Camera::SetFar(float far)
{
	mIsDirty = true;
	mFar = far;
}
//-----------------------------------------------------------------------------
void Camera::SetFovy(float fovy)
{
	mIsDirty = true;
	mFovy = fovy;
}
//-----------------------------------------------------------------------------
void Camera::SetAspect(float asp)
{
	mIsDirty = true;
	mAsp = asp;
}
//-----------------------------------------------------------------------------
void Camera::SetPosition(const glm::vec3& position)
{
	mIsDirty = true;
	mPosition = position;
}
//-----------------------------------------------------------------------------
void Camera::SetCenter(const glm::vec3& center)
{
	mIsDirty = true;
	mCenter = center;
}
//-----------------------------------------------------------------------------
void Camera::SetUp(const glm::vec3& up)
{
	mIsDirty = true;
	mUp = glm::normalize(up);
}
//-----------------------------------------------------------------------------
void Camera::Update() const
{
	// Compute camera frame.
	mZV = glm::normalize(mPosition - mCenter);
	mXV = glm::cross(mUp, mZV);
	mYV = glm::cross(mZV, mXV);

	// Compute camera transformations.
	mView = glm::lookAt(mPosition, mCenter, mUp);
	mViewInv = glm::inverse(mView);
	mViewNormal = glm::mat3(mView);
	mPerspective = glm::perspective(glm::radians(mFovy), mAsp, mNear, mFar);
	mPerspectiveInv = glm::inverse(mPerspective);
	mPerspectiveView = mPerspective * mView;
	mPerspectiveViewInv = glm::inverse(mPerspectiveView);

	// Other.
	mTanFovy2 = tanf(glm::radians(mFovy) * 0.5f);

	// Compute the frustum corners.
	glm::mat3 v(mViewInv);
	float x = mTanFovy2 * mAsp;
	float y = mTanFovy2;
	mFrustumDirections[0] = v * glm::vec3(-x, -y, -1.0);
	mFrustumDirections[1] = v * glm::vec3(+x, -y, -1.0);
	mFrustumDirections[2] = v * glm::vec3(+x, +y, -1.0);
	mFrustumDirections[3] = v * glm::vec3(-x, +y, -1.0);

	mIsDirty = false;
}
//-----------------------------------------------------------------------------
float Camera::GetTanFovy2() const
{
	if (mIsDirty)
		Update();
	return mTanFovy2;
}
//-----------------------------------------------------------------------------
const glm::mat4& Camera::GetPerspective() const
{
	if (mIsDirty) 
		Update();
	return mPerspective;
}
//-----------------------------------------------------------------------------
const glm::mat4& Camera::GetPerspectiveInv() const
{
	if (mIsDirty)
		Update();
	return mPerspectiveInv;
}
//-----------------------------------------------------------------------------
const glm::mat4& Camera::GetView() const
{
	if (mIsDirty)
		Update();
	return mView;
}
//-----------------------------------------------------------------------------
const glm::mat4& Camera::GetViewInv() const
{
	if (mIsDirty)
		Update();
	return mViewInv;
}
//-----------------------------------------------------------------------------
const glm::mat3& Camera::GetViewNormal() const
{
	if (mIsDirty)
		Update();
	return mViewNormal;
}
//-----------------------------------------------------------------------------
const glm::mat4& Camera::GetPerspectiveView() const
{
	if (mIsDirty)
		Update();
	return mPerspectiveView;
}
//-----------------------------------------------------------------------------
const glm::mat4& Camera::GetPerspectiveViewInv() const
{
	if (mIsDirty)
		Update();
	return mPerspectiveViewInv;
}
//-----------------------------------------------------------------------------
void Camera::Translate(const glm::vec3& translation)
{
	mPosition += translation;
	mCenter += translation;
	mIsDirty = true;
}
//-----------------------------------------------------------------------------
void Camera::GetFrustumPointsWC(glm::vec3 points[8]) const
{
	if (mIsDirty)
		Update();

	float a = mTanFovy2;
	float b = mAsp * a;
	glm::vec3 d[4];
	d[0] = -mZV + b * mXV + a * mYV;
	d[1] = -mZV + b * mXV - a * mYV;
	d[2] = -mZV - b * mXV + a * mYV;
	d[3] = -mZV - b * mXV - a * mYV;

	for (int i = 0; i < 4; i++) {
		points[i] = mPosition + mNear * d[i];
		points[4 + i] = mPosition + mFar * d[i];
	}
}
//-----------------------------------------------------------------------------
void Camera::RotateY(float angle)
{
	glm::vec3 viewDir = mCenter - mPosition;
	viewDir = glm::rotateY(viewDir, angle);
	mCenter = mPosition + viewDir;
	mIsDirty = true;
}
//-----------------------------------------------------------------------------
void Camera::MoveU(float x)
{
	glm::vec3 u = glm::normalize(glm::cross(mUp, (mCenter - mPosition)));
	mPosition += x * u;
	mCenter += x * u;
	mIsDirty = true;
}
//-----------------------------------------------------------------------------
void Camera::MoveN(float x)
{
	glm::vec3 n = glm::normalize((mCenter - mPosition));
	mPosition += x * n;
	mCenter += x * n;
	mIsDirty = true;
}
//-----------------------------------------------------------------------------
const glm::vec3* Camera::GetFrustumDirections() const
{
	if (mIsDirty)
		Update();
	return mFrustumDirections;
}
//-----------------------------------------------------------------------------
