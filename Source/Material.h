#ifndef MATERIAL_H
#define MATERIAL_H

#include "Entity.h"
#include <FxsGLCommon.hpp>

// Defines the material of a surface.
struct Material : public Entity<Material, 1024> {
	Material();
	~Material() {};
	
	glm::vec3 albedo;
	glm::vec3 reflectivity;
	float cosinePower;
};

#endif