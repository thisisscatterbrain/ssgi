#include "GBuffer.hpp"
#include "DeviceSurface.hpp"
#include "Camera.h"
#include "RenderCfg.h"

GBuffer GBuffer::instance;

//-----------------------------------------------------------------------------
GBuffer::GBuffer()
{
}
//-----------------------------------------------------------------------------
GBuffer::~GBuffer()
{
}
//-----------------------------------------------------------------------------
void GBuffer::Init()
{
	FXS_GL_ASSERT_SUCCESS();
	glm::uvec2 res = RenderCfg::instance.gBuffer.resolution;
	mProgram.InitWithFiles({"../Shader/GBuffer.vs", "../Shader/GBuffer.fs"});
	mDepths.Init(res, GL_DEPTH_COMPONENT32);
	mLinearDepths.Init(res, GL_R32F);
	mNormals.Init(res, GL_RGB10_A2);
	mAlbedos.Init(res, GL_RGB10_A2);
	mReflectivities.Init(res, GL_RGBA8);
	mFramebuffer.Init(res);
	mFramebuffer.Bind();
	mFramebuffer.Attach(GL_DEPTH_ATTACHMENT, mDepths);
	mFramebuffer.Attach(GL_COLOR_ATTACHMENT0, mLinearDepths);
	mFramebuffer.Attach(GL_COLOR_ATTACHMENT1, mNormals);
	mFramebuffer.Attach(GL_COLOR_ATTACHMENT2, mAlbedos);
	mFramebuffer.Attach(GL_COLOR_ATTACHMENT3, mReflectivities);
	GLenum drawBufs[] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1,
		GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3 };
	mFramebuffer.SetDrawBuffers(4, drawBufs);
}
//-----------------------------------------------------------------------------
void GBuffer::Deinit()
{
	mProgram.Deinit();
	mDepths.Deinit();
	mLinearDepths.Deinit();
	mNormals.Deinit();
	mAlbedos.Deinit();
	mReflectivities.Deinit();
	mFramebuffer.Deinit();
}
//-----------------------------------------------------------------------------
void GBuffer::Update()
{
	glEnable(GL_DEPTH_TEST);
	mFramebuffer.Bind();
	mFramebuffer.Clear();

	mProgram.Use();
	mProgram["camera.view"] = Camera.GetView();
	mProgram["camera.perspective"] = Camera.GetPerspective();

	for (const DeviceSurface& surf : DeviceSurface::pool) {
		mProgram["material.albedo"] = surf.GetMaterial().albedo;
		mProgram["material.reflectivity"] = surf.GetMaterial().reflectivity;
		mProgram["material.cosinePower"] = surf.GetMaterial().cosinePower;
		surf.Draw();
	}
	
	FXS_GL_ASSERT_SUCCESS();
}
//-----------------------------------------------------------------------------
