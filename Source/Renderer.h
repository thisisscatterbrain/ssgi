#ifndef RENDERER_H
#define RENDERER_H

#include "UpdateHandler.h"

// Interface to the render component.
class Renderer : public UpdateHandler {
public:
	void Init();
	void Deinit();
	void OnUpdate(float dt);
};

extern Renderer gRenderer;

#endif