#include "FullscreenQuad.hpp"

fxs::GLVertexArray FullscreenQuad::msVertexArray;
fxs::GLBuffer FullscreenQuad::msBuffer;
int FullscreenQuad::msReferenceCount = 0;

//-----------------------------------------------------------------------------
void FullscreenQuad::Init()
{
	if (msReferenceCount)
		return;

	static const GLfloat quad[] = {
		-1.0, -1.0,
		+1.0, -1.0,
		+1.0, +1.0,

		-1.0, -1.0,
		-1.0, +1.0,
		+1.0, +1.0
	};

	msBuffer.Init(GL_ARRAY_BUFFER, sizeof(quad), quad, GL_STATIC_DRAW);
	msVertexArray.Init(GL_TRIANGLES, 6);
	msVertexArray.Bind();
	msVertexArray.SetAttribute(msBuffer, 0, 2);
	msReferenceCount++;
}
//-----------------------------------------------------------------------------
void FullscreenQuad::Deinit()
{
	msReferenceCount--;

	if (msReferenceCount)
		return;

	msBuffer.Deinit();
	msVertexArray.Deinit();
}
//-----------------------------------------------------------------------------
void FullscreenQuad::Draw()
{
	msVertexArray.Bind();
	msVertexArray.DrawArrays();
}
//------------------------------------------------------------------------------