#ifndef APPLICATIONCFG_H
#define APPLICATIONCFG_H

#include <string>

struct ApplicationCfg {

	struct {
		int width;
		int height;
		std::string title;
	} window;

	// Loads default settings for the application.
	void Init();
	void Deinit();
};

extern ApplicationCfg appCfg;

#endif