#include "Light.h"
#include <FxsGLCommon.hpp>

DirectionalLight::DirectionalLight(const glm::vec3& direction, 
	const glm::vec3& color, float intensity)
	:
	invDirection(direction),
	color(color),
	intensity(intensity)
{
}

DirectionalLight::~DirectionalLight()
{
}

PointLight::PointLight(const glm::vec3& position, const glm::vec3& color, 
	float intensity, const glm::vec3& attenuation)
	:
	position(position),
	color(color),
	intensity(intensity),
	attenuation(attenuation)
{
}

PointLight::~PointLight()
{
}