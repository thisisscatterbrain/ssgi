#ifndef SSAOXRAD_HPP
#define SSAOXRAD_HPP

#include <FxsClassDecls.hpp>
#include <FxsGLProgram.hpp>
#include <FxsGLFramebuffer.hpp>
#include "FullscreenQuad.hpp"

class SSAOxRad {
	FXS_DECL_SINGLETON(SSAOxRad);
public:
	void Init();
	void Deinit();
	void Update();

	FXS_RO_MEMBER(fxs::GLTexture2D, IndirectDiffuse);
	FXS_RO_MEMBER(fxs::GLTexture2D, IndirectSpecular);
	FXS_RO_MEMBER(fxs::GLTexture2D, Glossyness);
	FXS_RO_MEMBER(fxs::GLTexture2D, Reflections);

private:
	fxs::GLProgram mProgram;
	fxs::GLFramebuffer mFramebuffer;

	fxs::GLProgram mProgramBlur;
	fxs::GLFramebuffer mFramebufferBlur;
	fxs::GLFramebuffer mFramebufferBlurTemp;
	fxs::GLTexture2D mIndirectDiffuseTemp;

	fxs::GLProgram mProgramRefl;
	fxs::GLFramebuffer mFramebufferRefl;


	fxs::GLProgram mProgramBlurSpec;
	fxs::GLFramebuffer mFramebufferBlurSpec;
	fxs::GLFramebuffer mFramebufferBlurSpecTemp;
	fxs::GLTexture2D mIndirectSpecularTemp;


	FullscreenQuad mFullscreenQuad;
};

#endif
