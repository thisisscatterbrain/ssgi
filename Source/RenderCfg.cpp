#include "RenderCfg.h"

RenderCfg RenderCfg::instance;

//-----------------------------------------------------------------------------
RenderCfg::RenderCfg()
{
}
//-----------------------------------------------------------------------------
RenderCfg::~RenderCfg()
{
}
//-----------------------------------------------------------------------------
void RenderCfg::Init()
{
	gBuffer.resolution = glm::uvec2(1280, 720);

	ssgi.radius = 0.3f;
	ssgi.sampleCount = 12;
	ssgi.aaoSigma = 1.0;
	ssgi.aaoK = 1.0;
	ssgi.aaoBeta = 0.0001;
	ssgi.aaoEps = 0.1;
	ssgi.logPatternCount = 2;
	ssgi.radAs = 6.0;

	ssgi.ssrDistMin = 0.1;
	ssgi.ssrDistMax = 3.0;
	ssgi.ssrRadius = 3.5;
	ssgi.ssrBlurRadius = 100.0;
	ssgi.ssrBlurSigmaS = 0.01;

	share.diffuse = 0.0;
	share.diffuseIndirect = 1.0;
	share.specularIndirect = 0.0;

	ssgi.blurRadius = 8;
	ssgi.blurSigmaS = 0.15;
	ssgi.blurSigmaR = 4.0;
	ssgi.blurSigmaR2 = 1.0;

	blur.radius = 0.0f;
	blur.sigmaS = 1.0f;
	blur.sigmaR = 0.0f;
}
//-----------------------------------------------------------------------------
void RenderCfg::Deinit()
{
}
//-----------------------------------------------------------------------------