#ifndef MESH_H
#define MESH_H

#include "Entity.h"
#include "Material.h"
#include <vector>

struct Mesh : public Entity<Mesh, 1024> {
	Mesh() {};
	~Mesh() {};
	
	int32_t GetVertexCount() const {return (int32_t)(positions.size());}
	int32_t GetFaceCount() const {return (int32_t)(positions.size() / 3);}
	
	const Material* material;
	std::vector<glm::vec3> positions;
	std::vector<glm::vec3> normals;
};

#endif