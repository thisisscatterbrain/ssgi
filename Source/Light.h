#ifndef LIGHT_H
#define LIGHT_H

#include "Entity.h"
#include <FxsGLCommon.hpp>

struct DirectionalLight : public Entity<DirectionalLight, 1024> {
	DirectionalLight(const glm::vec3& invDirection, const glm::vec3& color, 
		float intensity);
	~DirectionalLight();

	glm::vec3 invDirection;
	glm::vec3 color;
	float intensity;
};

struct PointLight : public Entity <PointLight, 1024> {
	PointLight(const glm::vec3& position, const glm::vec3& color, float intensity,
		const glm::vec3& attenuation);
	~PointLight();

	glm::vec3 position;
	glm::vec3 color;
	float intensity;
	glm::vec3 attenuation;
};

#endif