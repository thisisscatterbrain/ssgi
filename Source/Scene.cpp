#include "Scene.h"
#include "Camera.h"
#include "Mesh.h"
#include "Light.h"
#include <tinyxml2.h>
#include <FxsLog.hpp>

using namespace tinyxml2;

Scene gScene;

extern void LoadObj(const char* filename, const char* path);

//-----------------------------------------------------------------------------
static glm::uvec2 GetUVec2FromAttributeValue(const char* value)
{
	glm::uvec2 v;
	sscanf(value, "%u %u", &v[0], &v[1]);
	return v;
}
//-----------------------------------------------------------------------------
static glm::vec3 GetVec3FromAttributeValue(const char* value) 
{
	glm::vec3 v;
	sscanf(value, "%f %f %f", &v[0], &v[1], &v[2]);
	return v;
}
//-----------------------------------------------------------------------------
static float GetFloatFromAttributeValue(const char* value)
{
	float f;
	sscanf(value, "%f", &f);
	return f;
}
//-----------------------------------------------------------------------------
static const XMLElement* FindFirstChildWithName(const XMLElement& element, 
	const char* name)
{
	const XMLElement* elem = element.FirstChildElement();
	
	while (elem) {
		if (0 == strcmp(elem->Attribute("name"), name))
			return elem;
		elem = elem->NextSiblingElement();
	}
	return nullptr;
}
//-----------------------------------------------------------------------------
static glm::vec3 Vec3FromChild(const XMLElement& parent, const char* name)
{
	const XMLElement* elem = FindFirstChildWithName(parent, name);

	if (!elem) {
		FXS_LOG("WARNING: Element with name \"%s\" not found.", name);
		return glm::vec3(0.0);
	}

	if (0 != strcmp(elem->Name(), "vec3")) {
		FXS_LOG("WARNING: Element with name \"%s\" is not a vec3.", name);
		return glm::vec3(0.0);
	}
	return GetVec3FromAttributeValue(elem->Attribute("value"));
}
//-----------------------------------------------------------------------------
static glm::uvec2 UVec2FromChild(const XMLElement& parent, const char* name)
{

	const XMLElement* elem = FindFirstChildWithName(parent, name);

	if (!elem) {
		FXS_LOG("WARNING: Element with name \"%s\" not found.", name);
		return glm::uvec2(0);
	}

	if (0 != strcmp(elem->Name(), "uvec2")) {
		FXS_LOG("WARNING: Element with name \"%s\" is not a uvec2.", name);
		return glm::uvec2(0);
	}
	return GetUVec2FromAttributeValue(elem->Attribute("value"));
}
//-----------------------------------------------------------------------------
static float FloatFromChild(const XMLElement& parent, const char* name)
{
	const XMLElement* elem = FindFirstChildWithName(parent, name);

	if (!elem) {
		FXS_LOG("WARNING: Element with name \"%s\" not found.", name);
		return 0.0f;
	}

	if (0 != strcmp(elem->Name(), "float")) {
		FXS_LOG("WARNING: Element with name \"%s\" is not a vec3.", name);
		return 0.0f;
	}
	return GetFloatFromAttributeValue(elem->Attribute("value"));
}
//-----------------------------------------------------------------------------
static bool IsType(const XMLElement& element, const char* type)
{
	return (0 == strcmp(element.Name(), type));
}
//-----------------------------------------------------------------------------
static void InitCamera(const XMLNode& node)
{
	const XMLElement* elem = node.ToElement();
	Camera.SetPosition(Vec3FromChild(*elem, "position"));
	Camera.SetCenter(Vec3FromChild(*elem, "center"));
	Camera.SetUp(Vec3FromChild(*elem, "up"));
	Camera.SetNear(FloatFromChild(*elem, "near"));
	Camera.SetFar(FloatFromChild(*elem, "far"));
	Camera.SetFovy(FloatFromChild(*elem, "fovy"));
	Camera.SetAspect(FloatFromChild(*elem, "aspect"));
}
//-----------------------------------------------------------------------------
static void LoadMeshFromObj(const XMLElement& element)
{
	const XMLElement* elem = element.FirstChildElement("obj");
	const char* name = "";
	
	if (element.Attribute("name")) 
		name = element.Attribute("name");

	if (!elem) {
		FXS_LOG("WARNING: [obj] element not found for mesh element with name \"%s\".", 
			name);
		return;
	}

	const XMLElement* elem2;
	const char* path = "./";
	elem2 = FindFirstChildWithName(*elem, "path");

	if (elem2 && elem2->Attribute("value"))
		path = elem2->Attribute("value");
		
	const char* filen = nullptr;
	elem2 = FindFirstChildWithName(*elem, "filename");

	if (elem2 && elem2->Attribute("value")) {
		filen = elem2->Attribute("value"); 
	} else {
		FXS_LOG("WARNING :.obj filename not found for mesh with name \"%s\".",
			name);
		return;
	}

	LoadObj(filen, path);
}
//-----------------------------------------------------------------------------
static void LoadDirectionalLight(const XMLElement& element)
{
	glm::vec3 dir = glm::normalize(Vec3FromChild(element, "direction"));
	glm::vec3 col = Vec3FromChild(element, "color");
	float intens = FloatFromChild(element, "intensity");	
	new DirectionalLight(dir, col, intens);
}
//-----------------------------------------------------------------------------
static void LoadPointLight(const XMLElement& element)
{
	glm::vec3 pos = Vec3FromChild(element, "position");
	glm::vec3 col = Vec3FromChild(element, "color");
	float intens = FloatFromChild(element, "intensity");
	glm::vec3 atten = Vec3FromChild(element, "attenuation");
	new PointLight(pos, col, intens, atten);
}
//-----------------------------------------------------------------------------
static void LoadEntities(const XMLNode& node) {
	const XMLElement* elem = node.ToElement();

	if (IsType(*elem, "camera")) {
		InitCamera(node);
	} else if (IsType(*elem, "mesh")) {
		if (elem->Attribute("format"), ".obj") {
			LoadMeshFromObj(*elem);
		}
	} else if (IsType(*elem, "light")) {
		if (elem->Attribute("type", "directional"))
			LoadDirectionalLight(*elem);
		else if (elem->Attribute("type", "point"))
			LoadPointLight(*elem);
		else 
			FXS_LOG("WARNING: Light type not supported", "");
	}

	const XMLNode* n = node.FirstChild();

	while (n) {
		LoadEntities(*n);
		n = n->NextSibling();
	}
}
//-----------------------------------------------------------------------------
void Scene::Load(const std::string& filename)
{
	XMLDocument doc;
	XMLError err = doc.LoadFile(filename.c_str());
	
	if (err == XML_ERROR_FILE_NOT_FOUND) {
		FXS_LOG("WARNING: Scene file with filename \"%s\" not found.",
			filename.c_str());
		return;
	} else if (err != XML_SUCCESS) {
		FXS_LOG(doc.GetErrorStr1(), "");
		return;
	}

	XMLNode* root = doc.FirstChild();

	if (!root || (0 != strcmp(root->ToElement()->Name(), "scene"))) {
		FXS_LOG("[scene] node is not root.", "");
		return;
	}

	LoadEntities(*root);
}
//-----------------------------------------------------------------------------