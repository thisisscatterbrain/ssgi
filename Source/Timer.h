#ifndef TIMER_H
#define TIMER_H

#include <string>

void TimerStart(const std::string& id);
void TimerStop(const std::string& id);

// Gets the elapsed time for a timer in [ms].
double TimerGetElapsed(const std::string& id);

#define TIMER_START(id) TimerStart(id);
#define TIMER_STOP(id) TimerStop(id);
#define TIMER_GET_ELAPSED(id) TimerGetElapsed(id);


#endif TIMER_H