#include "ParameterTweaker.h"
#include "Assert.h"
#include "Application.h"
#include "ApplicationCfg.h"
#include "RenderCfg.h"
#include <GLFW/glfw3.h>
#include <AntTweakBar.h>
#include <FxsAssert.hpp>

static TwBar* tweakBarSSGI = nullptr;
static TwBar* tweakBarBlur = nullptr;

//-----------------------------------------------------------------------------
static void CursorPosFun(GLFWwindow* window, double x, double y)
{
	TwEventMousePosGLFW(x, y);
}
//-----------------------------------------------------------------------------
static void MouseButtonFun(GLFWwindow* window, int button, int action, int mods)
{
	TwEventMouseButtonGLFW(button, action);
}
//-----------------------------------------------------------------------------
void ParameterTweakerInit()
{
	int succ = TwInit(TW_OPENGL_CORE, NULL);
	FXS_ASSERT(1 == succ);
	TwWindowSize(appCfg.window.width, appCfg.window.height);

	TwAddSeparator(tweakBarSSGI, NULL, " group='SSAOxDiffuse' ");

	tweakBarSSGI = TwNewBar("SSGI Parameters");
	TwAddVarRW(tweakBarSSGI, "Radius", TW_TYPE_FLOAT,
		&RenderCfg::instance.ssgi.radius,
		" label='Radius' min=0 max=4 step=0.01 help='Time (in seconds).' group='SSAOxDiffuse' ");
	TwAddVarRW(tweakBarSSGI, "SampleCount", TW_TYPE_INT32,
		&RenderCfg::instance.ssgi.sampleCount,
		" label='SampleCount' min=1 max=256 step=1 help='Time (in seconds).' group='SSAOxDiffuse' ");
	TwAddVarRW(tweakBarSSGI, "AAO_Sigma", TW_TYPE_FLOAT, 
		&RenderCfg::instance.ssgi.aaoSigma,
		" label='AAO_Sigma' min=0 max=4 step=0.01 help='Time (in seconds).' group='SSAOxDiffuse' ");
	TwAddVarRW(tweakBarSSGI, "AAO_k", TW_TYPE_FLOAT,
		&RenderCfg::instance.ssgi.aaoK,
		" label='AAO_k' min=0 max=4 step=0.01 help='Time (in seconds).' group='SSAOxDiffuse' ");
	TwAddVarRW(tweakBarSSGI, "AAO_eps", TW_TYPE_FLOAT,
		&RenderCfg::instance.ssgi.aaoEps,
		" label='AAO_eps' min=0 max=4 step=0.0001 help='Time (in seconds).' group='SSAOxDiffuse' ");
	TwAddVarRW(tweakBarSSGI, "AAO_beta", TW_TYPE_FLOAT,
		&RenderCfg::instance.ssgi.aaoBeta,
		" label='AAO_beta' min=0 max=4 step=0.0001 help='Time (in seconds).' group='SSAOxDiffuse' ");
	TwAddVarRW(tweakBarSSGI, "Rad_As", TW_TYPE_FLOAT,
		&RenderCfg::instance.ssgi.radAs,
		" label='Rad_As' min=0 max=32 step=0.01 help='Time (in seconds).' group='SSAOxDiffuse' ");
	TwAddVarRW(tweakBarSSGI, "Blur_sigmaS", TW_TYPE_FLOAT,
		&RenderCfg::instance.ssgi.blurSigmaS,
		" label='Blur_sigmaS' min=0 max=40 step=0.01 help='Time (in seconds).' group='SSAOxDiffuse' ");
	TwAddVarRW(tweakBarSSGI, "Blur_sigmaR", TW_TYPE_FLOAT,
		&RenderCfg::instance.ssgi.blurSigmaR,
		" label='Blur_sigmaR' min=0 max=40 step=0.01 help='Time (in seconds).' group='SSAOxDiffuse' ");
	TwAddVarRW(tweakBarSSGI, "Blur_sigmaR2", TW_TYPE_FLOAT,
		&RenderCfg::instance.ssgi.blurSigmaR2,
		" label='Blur_sigmaR2' min=0 max=40 step=0.01 help='Time (in seconds).' group='SSAOxDiffuse' ");
	TwAddVarRW(tweakBarSSGI, "Blur_radius", TW_TYPE_INT32,
		&RenderCfg::instance.ssgi.blurRadius,
		" label='Blur_radius' min=0 max=64 step=1.00 help='Time (in seconds).' group='SSAOxDiffuse' ");

	TwAddSeparator(tweakBarSSGI, NULL, " group='SSR' ");
	
	TwAddVarRW(tweakBarSSGI, "SSR_BlurSigmaS", TW_TYPE_FLOAT,
		&RenderCfg::instance.ssgi.ssrBlurSigmaS,
		" label='SSR_BlurSigmaS' min=0.01 max=100.0 step=0.01 help='Time (in seconds).' group='SSR' ");
	TwAddVarRW(tweakBarSSGI, "SSR_BlurRadius", TW_TYPE_FLOAT,
		&RenderCfg::instance.ssgi.ssrBlurRadius,
		" label='SSR_BlurRadius' min=1.0 max=100.0 step=0.01 help='Time (in seconds).' group='SSR' ");
	TwAddVarRW(tweakBarSSGI, "SSR_DistMin", TW_TYPE_FLOAT,
		&RenderCfg::instance.ssgi.ssrDistMin,
		" label='SSR_DistMin' min=0.00 max=100.0 step=0.001 help='Time (in seconds).' group='SSR' ");
	TwAddVarRW(tweakBarSSGI, "SSR_DistMax", TW_TYPE_FLOAT,
		&RenderCfg::instance.ssgi.ssrDistMax,
		" label='SSR_DistMax' min=0.00 max=100.0 step=0.001 help='Time (in seconds).' group='SSR' ");

	TwAddVarRW(tweakBarSSGI, "Compose_diffuse", TW_TYPE_FLOAT,
		&RenderCfg::instance.share.diffuse,
		" label='Compose_diffuse' min=0 max=4 step=0.1 help='Time (in seconds).' ");
	TwAddVarRW(tweakBarSSGI, "Compose_diffuseIndirect", TW_TYPE_FLOAT,
		&RenderCfg::instance.share.diffuseIndirect,
		" label='Compose_diffuseIndirect' min=0 max=4 step=0.1 help='Time (in seconds).' ");
	TwAddVarRW(tweakBarSSGI, "Compose_specularIndirect", TW_TYPE_FLOAT,
		&RenderCfg::instance.share.specularIndirect,
		" label='Compose_specularIndirect' min=0 max=4 step=0.1 help='Time (in seconds).' ");

	GLFWwindow* window = gApplication.window;
	FXS_ASSERT(window);
	glfwSetMouseButtonCallback(window, MouseButtonFun);
	glfwSetCursorPosCallback(window, CursorPosFun);
	glfwSetCharCallback(window, (GLFWcharfun)TwEventCharGLFW);
}
//----------------------------------------------------------------------------
void ParameterTweakerDeinit()
{
	TwTerminate();
}
//----------------------------------------------------------------------------
void ParameterTweakerUpdate()
{
	TwDraw();
}
//----------------------------------------------------------------------------
