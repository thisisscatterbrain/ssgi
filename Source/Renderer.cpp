#include "Renderer.h"
#include "RenderCfg.h"
#include "Application.h"
#include "DeviceSurface.hpp"
#include "GBuffer.hpp"
#include "RadiosityBuffer.hpp"
#include "SSAOxRad.hpp"
#include "Compose.hpp"
#include "ParameterTweaker.h"
#include <FxsProfiler.hpp>

Renderer gRenderer;

//-----------------------------------------------------------------------------
void Renderer::Init()
{
	RenderCfg::instance.Init();
	GBuffer::instance.Init();
	RadiosityBuffer::instance.Init();
	SSAOxRad::instance.Init();
	Compose::instance.Init();

	for (const Mesh& mesh : Mesh::pool) {
		new DeviceSurface(mesh);
	}

	gApplication.AddUpdateHandler(*this);

	PARAMETER_TWEAKER_INIT
}
//-----------------------------------------------------------------------------
void Renderer::Deinit()
{
	PARAMETER_TWEAKER_DEINIT

	GBuffer::instance.Deinit();
	RadiosityBuffer::instance.Deinit();
	SSAOxRad::instance.Deinit();
	Compose::instance.Deinit();
	DeviceSurface::pool.Reset();
}
//-----------------------------------------------------------------------------
void Renderer::OnUpdate(float dt)
{
	FXS_PROFILER_GL_START("GBuffer");
	GBuffer::instance.Update();
	FXS_PROFILER_GL_STOP("GBuffer");

	RadiosityBuffer::instance.Update();
	SSAOxRad::instance.Update();
	Compose::instance.Update();

	PARAMETER_TWEAKER_UPDATE
}
//-----------------------------------------------------------------------------