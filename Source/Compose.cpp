#include "Compose.hpp"
#include "RenderCfg.h"
#include "GBuffer.hpp"
#include "SSAOxRad.hpp"
#include "RadiosityBuffer.hpp"

Compose Compose::instance;

//-----------------------------------------------------------------------------
Compose::Compose()
{
}
//-----------------------------------------------------------------------------
Compose::~Compose()
{
}
//-----------------------------------------------------------------------------
void Compose::Init()
{
	mFullscreenQuad.Init();
	mProgram.InitWithFiles({ "../Shader/Fullscreen.vs", "../Shader/Compose.fs"});
	mProgram.Use();
	mProgram["lightMaps.resolution"] = RenderCfg::instance.gBuffer.resolution;
	mProgram["lightMaps.indirectDiffuse"] = 0;
	mProgram["lightMaps.radiosities"] = 1;
	mProgram["lightMaps.indirectSpecular"] = 2;
#ifndef TWEAKING_ENABLED
	mProgram["share.diffuse"] = RenderCfg::instance.share.diffuse;
	mProgram["share.indirectDiffuse"] = RenderCfg::instance.share.diffuseIndirect;
	mProgram["share.indirectSpecular"] = RenderCfg::instance.share.specularIndirect;
#endif

}
//-----------------------------------------------------------------------------
void Compose::Deinit()
{
	mProgram.Deinit();
	mFullscreenQuad.Deinit();
}
//-----------------------------------------------------------------------------
void Compose::Update()
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glClear(GL_COLOR_BUFFER_BIT);
	mProgram.Use();

#ifdef TWEAKING_ENABLED
	mProgram["share.diffuse"] = RenderCfg::instance.share.diffuse;
	mProgram["share.indirectDiffuse"] = RenderCfg::instance.share.diffuseIndirect;
	mProgram["share.indirectSpecular"] = RenderCfg::instance.share.specularIndirect;
#endif

	SSAOxRad::instance.GetIndirectDiffuse().Bind(0);
	RadiosityBuffer::instance.GetRadiosities().Bind(1);
	SSAOxRad::instance.GetReflections().Bind(2);
	mFullscreenQuad.Draw();
}
//-----------------------------------------------------------------------------