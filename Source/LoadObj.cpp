#include "Mesh.h"
#include "Material.h"
#include <FxsLog.hpp>
#include <FxsGLCommon.hpp>
#include <vector>

// Aux. structs for loading meshes.

struct VertexData {
	std::vector<glm::vec3> positions;
	std::vector<glm::vec3> normals;
	std::vector<glm::vec2> uvs;
};

struct MaterialEntry {
	char name[256];
	Material* material;
};

struct ObjHeader {
	VertexData vertexData;
	std::vector<MaterialEntry> materialEntries;
};

//-----------------------------------------------------------------------------
static Material* GetMaterialForKey(std::vector<MaterialEntry> materialEntries,
	const char* key)
{
	int32_t id = 0;
	
	for (MaterialEntry& matEnt : materialEntries) {
		if (0 == strcmp(matEnt.name, key)) 
			return matEnt.material;
		id++;
	}
	return nullptr;
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
static void ReadVec3(glm::vec3* v, const char* prefix, const char* l)
{
	char format[32];
	sprintf(format, "%s %%f %%f %%f", prefix);

	if (3 != sscanf(l, format, &(*v)[0], &(*v)[1], &(*v)[2])) {
		FXS_LOG("WARNING: Corrupted line.", "");
	}
}
//-----------------------------------------------------------------------------
static void ReadVec2(glm::vec2* v, const char* prefix, const char* l)
{
	char format[32];
	sprintf(format, "%s %%f %%f", prefix);

	if (2 != sscanf(l, format, &(*v)[0], &(*v)[1])) {
		FXS_LOG("WARNING: Corrupted line.", "");
	}
}
//-----------------------------------------------------------------------------
static void ReadFloat(float* f, const char* prefix, const char* l)
{
	char format[32];
	sprintf(format, "%s %%f", prefix);

	if (1 != sscanf(l, format, f)) {
		FXS_LOG("WARNING: Corrupted line.", "");
	}
}
//-----------------------------------------------------------------------------
static void LoadMaterialEntries(std::vector<MaterialEntry>* entries, FILE* file)
{
	char line[256];

	while (fgets(line, sizeof(line), file)) {
		if (strstr(line, "newmtl ") == line) {
			MaterialEntry matEnt;
			sscanf(line, "newmtl %s", matEnt.name);
			matEnt.material = new Material();
			entries->push_back(matEnt);
		} else if (strstr(line, "Kd") == line) {
			MaterialEntry& matEnt = entries->back();
			ReadVec3(&(matEnt.material->albedo), "Kd", line);
		} else if (strstr(line, "Ks") == line) {
			MaterialEntry& matEnt = entries->back();
			ReadVec3(&(matEnt.material->reflectivity), "Ks", line);
		} else if (strstr(line, "Ns") == line) {
			MaterialEntry& matEnt = entries->back();
			ReadFloat(&(matEnt.material->cosinePower), "Ns", line);
		}
	}
}
//-----------------------------------------------------------------------------
static void InitHeader(ObjHeader* header, FILE* file, const char* path)
{
	char line[256];
	glm::vec3 v3;
	glm::vec2 v2;

	while (fgets(line, sizeof(line), file)) {
		if (strstr(line, "v ") == line) {
			ReadVec3(&v3, "v", line);
			header->vertexData.positions.push_back(v3);
		} else if (strstr(line, "vn ") == line) {
			ReadVec3(&v3, "vn", line);
			header->vertexData.normals.push_back(v3);
		} else if (strstr(line, "vt ") == line) {
			ReadVec2(&v2, "vt", line);
			header->vertexData.uvs.push_back(v2);
		} else if (strstr(line, "mtllib") == line) {
			char filen[256];

			if (1 != sscanf(line, "mtllib %s", filen)) {
				FXS_LOG("WARNING: Failed to load the .mtl file.", "");
				break;
			}

			char fullFilen[256];
			sprintf(fullFilen, "%s/%s", path, filen);
			FILE* f = fopen(fullFilen, "r");
		
			if (!f) {
				FXS_LOG("WARNING: Failed to open the .mtl file with filename %s.", 
					fullFilen);
				break;
			}

			LoadMaterialEntries(&(header->materialEntries), f);
			fclose(f);
		}
	}
}
//-----------------------------------------------------------------------------
static void AddFace(Mesh* mesh, const VertexData& data, const char* line)
{
	uint32_t p0, p1, p2;
	uint32_t n0 = 0, n1 = 0, n2 = 0;
	uint32_t uv0 = 0, uv1 = 0, uv2 = 0;
	int scanRes = sscanf(line, "f %u %u %u", &p0, &p1, &p2);

	if (scanRes == 3) {
		goto addFace;
	}

	scanRes = sscanf(line, "f %u/%u %u/%u %u/%u",
		&p0, &uv0, &p1, &uv1, &p2, &uv2);

	if (scanRes == 6) {
		goto addFace;
	}

	scanRes = sscanf(line, "f %u/%u/ %u/%u/ %u/%u/",
		&p0, &uv0, &p1, &uv1, &p2, &uv2);

	if (scanRes == 6) {
		goto addFace;
	}

	scanRes = sscanf(line, "f %u//%u %u//%u %u//%u",
		&p0, &n0, &p1, &n1, &p2, &n2);

	if (scanRes == 6) {
		goto addFace;
	}

	scanRes = sscanf(line, "f %u/%u/%u %u/%u/%u %u/%u/%u",
		&p0, &uv0, &n0, &p1, &uv1, &n1, &p2, &uv2, &n2);

	if (scanRes == 9) {
		goto addFace;
	}

	FXS_LOG("WARNING: Invalid face.", "")

addFace:
	mesh->positions.push_back(data.positions[p0 - 1]);
	mesh->positions.push_back(data.positions[p1 - 1]);
	mesh->positions.push_back(data.positions[p2 - 1]);

	if (n0)
		mesh->normals.push_back(glm::normalize(data.normals[n0 - 1]));

	if (n1)
		mesh->normals.push_back(glm::normalize(data.normals[n1 - 1]));
	
	if (n2)
		mesh->normals.push_back(glm::normalize(data.normals[n2 - 1]));
}
//-----------------------------------------------------------------------------
static void LoadMeshes(const ObjHeader& header, FILE* file)
{
	char line[256];
	Mesh* mesh = nullptr;			// Currently updating mesh.
	Material* mat = new Material(); // Default material.

	while (fgets(line, sizeof(line), file) == line) {
		if (strstr(line, "usemtl ") == line) {
			char key[256];
			sscanf(line, "usemtl %s", key);
			mat = GetMaterialForKey(header.materialEntries, key);
			if (!mesh || (mesh->GetFaceCount() > 0)) {
				mesh = new Mesh();
			}
			mesh->material = mat;
		}

		if (strstr(line, "o ") || strstr(line, "g ")) {
			if (!mesh || (mesh->GetFaceCount() > 0)) {
				mesh = new Mesh();
				mesh->material = mat;
			}
		} else if (strstr(line, "f ") == line) {
			if (!mesh) {
				// Take the possibility that no groups, "material-groups", or  
				// objects exist into account.
				mesh = new Mesh();
			}
			AddFace(mesh, header.vertexData, line);
		} 
	}
}
//-----------------------------------------------------------------------------
void LoadObj(const char* filename, const char* path)
{
	char fullFilen[256];
	sprintf(fullFilen, "%s/%s", path, filename);

	FILE* f = fopen(fullFilen, "r");

	if (!f) {
		FXS_LOG("WARNING: .obj file with filename \"%s\" not found", fullFilen);
		return;
	}

	FXS_LOG("Loading \"%s\"... \n\tNOTE: Only triangle meshes are supported correctly.", 
		fullFilen);

	// 1st pass : Gather information needed to initialize meshes.
	// NOTE: Already initializes materials.
	ObjHeader header;
	InitHeader(&header, f, path);
	rewind(f);
	
	// 2nd pass : Load the actual meshes and associates them with their
	// materials.
	LoadMeshes(header, f);
	fclose(f);
}
//-----------------------------------------------------------------------------