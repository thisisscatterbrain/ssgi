#ifndef COMPOSE_HPP
#define COMPOSE_HPP

#include <FxsClassDecls.hpp>
#include <FxsGLProgram.hpp>
#include "FullscreenQuad.hpp"

class Compose {
	FXS_DECL_SINGLETON(Compose);
public:
	void Init();
	void Deinit();
	void Update();

private:
	fxs::GLProgram mProgram;
	FullscreenQuad mFullscreenQuad;
};

#endif