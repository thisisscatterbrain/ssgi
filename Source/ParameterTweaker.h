#ifndef PARAMETERTWEAKER_H
#define PARAMETERTWEAKER_H

void ParameterTweakerInit();
void ParameterTweakerDeinit();
void ParameterTweakerUpdate();

#ifdef TWEAKING_ENABLED
	#define PARAMETER_TWEAKER_INIT		ParameterTweakerInit();
	#define PARAMETER_TWEAKER_DEINIT	ParameterTweakerDeinit();
	#define PARAMETER_TWEAKER_UPDATE	ParameterTweakerUpdate();
#else
	#define PARAMETER_TWEAKER_INIT
	#define PARAMETER_TWEAKER_DEINIT
	#define PARAMETER_TWEAKER_UPDATE
#endif

#endif