#include "Application.h"
#include "Scene.h"
#include "Renderer.h"
#include <FxsProfiler.hpp>

void Pause()
{
	std::system("pause");
}

int main(int argc, char* argv[])
{
	atexit(Pause);
	FXS_PROFILER_INIT("../Output/Profiler.dat");
	gScene.Load("../Resources/Scenes/BunnyBox03.xml");
	gApplication.Init();
	gRenderer.Init();
	gApplication.Run();
	gRenderer.Deinit();
	gApplication.Deinit();
	FXS_PROFILER_DEINIT();
	return 0;
}