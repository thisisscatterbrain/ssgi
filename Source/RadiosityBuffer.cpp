#include "RadiosityBuffer.hpp"
#include "RenderCfg.h"
#include "Camera.h"
#include "Light.h"
#include "RenderCfg.h"
#include "GBuffer.hpp"

RadiosityBuffer RadiosityBuffer::instance;

//-----------------------------------------------------------------------------
RadiosityBuffer::RadiosityBuffer()
{
}
//-----------------------------------------------------------------------------
RadiosityBuffer::~RadiosityBuffer()
{
}
//-----------------------------------------------------------------------------
void RadiosityBuffer::Init()
{
	mProgram.InitWithFiles({ "../Shader/FullscreenPFD.vs", 
		"../Shader/RadiosityBuffer.fs" });
	mProgram.Use();
	mProgram["gBuffer.resolution"] = RenderCfg::instance.gBuffer.resolution;
	mProgram["gBuffer.linearDepths"] = 0;
	mProgram["gBuffer.normals"] = 1;
	mProgram["gBuffer.albedos"] = 2;

	glm::uvec2& res = RenderCfg::instance.gBuffer.resolution;
	mRadiosities.Init(res, GL_RGB16F);
	mFramebuffer.Init(res);
	mFramebuffer.Bind();
	mFramebuffer.Attach(GL_COLOR_ATTACHMENT0, mRadiosities);
	GLenum drawBufs[] = { GL_COLOR_ATTACHMENT0 };
	mFramebuffer.SetDrawBuffers(1, drawBufs);
	mFullscreenQuad.Init();
	FXS_GL_ASSERT_SUCCESS();
}
//-----------------------------------------------------------------------------
void RadiosityBuffer::Deinit()
{
	mRadiosities.Deinit();
	mProgram.Deinit();
	mFramebuffer.Deinit();
	mFullscreenQuad.Deinit();
}
//-----------------------------------------------------------------------------
void RadiosityBuffer::Update()
{
	glDisable(GL_DEPTH_TEST);
	mFramebuffer.Bind();
	mProgram.Use();
	mProgram["camera.position"] = Camera.GetPosition();
	mProgram["camera.perspectiveViewInv"] = Camera.GetPerspectiveViewInv();
	SetLights();
	GBuffer::instance.GetLinearDepths().Bind(0);
	GBuffer::instance.GetNormals().Bind(1);
	GBuffer::instance.GetAlbedos().Bind(2);
	mFullscreenQuad.Draw();
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void RadiosityBuffer::SetLights()
{
	GLchar varn[1024];
	int i = 0;
	for (const PointLight& pl : PointLight::pool) {
		sprintf(varn, "pointLights[%i].position", i);
		mProgram[varn] = pl.position;
		sprintf(varn, "pointLights[%i].color", i);
		mProgram[varn] = pl.color;
		sprintf(varn, "pointLights[%i].intensity", i);
		mProgram[varn] = pl.intensity;
		sprintf(varn, "pointLights[%i].attenuation", i);
		mProgram[varn] = pl.attenuation;
		i++;
	}
	mProgram["pointLightCount"] = int(PointLight::pool.GetCount());
}
//-----------------------------------------------------------------------------
