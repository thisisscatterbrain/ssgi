#version 410 core

layout(location = 0) out vec3 fSpecular;

uniform vec2 resolution;

uniform struct {
	float sigmaS;
	float radius;
	vec2 direction;
} blur;

uniform sampler2D reflectivity;
uniform sampler2D indirectSpecular;
uniform sampler2D glossyness;


void main()
{
	vec2 uv = gl_FragCoord.xy / resolution;
	float g = texture(glossyness, uv).r;
	float r = g * blur.radius;
	vec3 spec = vec3(0.0);
	float w = 0.0;	


	for (float i = -r; i <= r; i += 1.0) {
		vec2 uvn = (gl_FragCoord.xy + i * blur.direction) / resolution;

		vec4 refl = texture(reflectivity, uv);

		float rs = i * blur.sigmaS;
		float ws = exp(-(rs * rs));
		spec += ws * refl.rgb * texture(indirectSpecular, uvn).rgb;
		w += ws;
	}

	fSpecular = 2.0* spec / w;
}