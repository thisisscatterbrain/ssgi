#version 410

in vec3 vDirectionWS;

layout(location = 0) out vec3 fRadiosity;

#define MAX_POINT_LIGHT_COUNT 8

uniform struct {
	vec2 resolution;
	sampler2D linearDepths;
	sampler2D normals;
	sampler2D albedos;
} gBuffer;

uniform struct {
	vec3 position;
	vec3 color;
	float intensity;
	vec3 attenuation;
} pointLights[MAX_POINT_LIGHT_COUNT];

uniform int pointLightCount;

uniform struct {
	vec3 position;
} camera;

//-----------------------------------------------------------------------------
vec3 GetPositionWS(vec2 uv)
{
	float zDist = texture(gBuffer.linearDepths, uv).r;
	return camera.position + zDist * vDirectionWS;
}
//-----------------------------------------------------------------------------
vec3 GetNormalWS(vec2 uv)
{
	return texture(gBuffer.normals, uv).xyz * 2.0 - 1.0;
}
//-----------------------------------------------------------------------------
void main()
{
	vec2 uv = gl_FragCoord.xy / gBuffer.resolution;
	vec3 p = GetPositionWS(uv);
	vec3 n = GetNormalWS(uv);
	vec3 alb = texture(gBuffer.albedos, uv).rgb;
	vec3 Lo = vec3(0.0);

	for (int i = 0; i < pointLightCount; i++) {
		vec3 pl = pointLights[i].position;
		vec3 d = pl - p;
		float dist = length(d);
		d /= dist;
		vec3 al = pointLights[i].attenuation;
		float att = al[0] + dist * (al[1] + dist * al[2]);
		Lo += alb * pointLights[i].color * pointLights[i].intensity / att *
			dot(n, d);
		
	}

	fRadiosity = Lo;
}
//----------------------------------------------------------------------------- 