#version 410 core

in vec3 vFrustumDirection;
layout(location = 0) out vec3 fRadiosity;

uniform struct {
	vec2 resolution;
	sampler2D linearDepths;
	sampler2D normals;
	sampler2D albedo;
} gBuffer;

uniform struct {
	float near;
	float far;
	vec3 position;
} camera;

#define MAX_POINT_LIGHT_COUNT 4

uniform struct {
	vec3 position;
	vec3 color;
	float intensity;
	vec3 attenuation;
} pointLights[MAX_POINT_LIGHT_COUNT];

uniform int pointLightCount;

vec3 ComputeRadiosity(vec3 position, vec3 normal, vec3 albedo)
{
	vec3 L = vec3(0.0);

	for (int i = 0; i < pointLightCount; i++) {
		vec3 d = pointLights[i].position - position;
		float l = length(d);
		d /= l;
		float a = pointLights[i].attenuation.x + l * (pointLights[i].attenuation.y + pointLights[i].attenuation.z * l);
		vec3 E = pointLights[i].intensity * pointLights[i].color / a;
		L += E * max(0.0, dot(normal, d));
	}
	return L * albedo;
}

vec3 NormalFromUV(vec2 uv)
{
	return texture(gBuffer.normals, uv).xyz * 2.0 - 1.0;
}

vec3 PositionFromLinearDepth(float linearDepth)
{
	float mZVC = linearDepth * (camera.far - camera.near) + camera.near;
	return camera.position + mZVC * vFrustumDirection;
}

void main()
{
	vec2 uv = gl_FragCoord.xy / gBuffer.resolution;
	float ld = texture(gBuffer.linearDepths, uv).r;
	
	if (ld >= 1.0)
		discard;

	vec3 p = PositionFromLinearDepth(ld);
	vec3 n = NormalFromUV(uv);
	vec3 alb = texture(gBuffer.albedo, uv).rgb;
	fRadiosity = ComputeRadiosity(p, n, alb);
}