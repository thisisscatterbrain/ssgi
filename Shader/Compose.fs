#version 410 core

layout(location = 0) out vec3 fColor;

uniform struct {
	vec2 resolution;
	sampler2D indirectDiffuse;
	sampler2D indirectSpecular;
	sampler2D radiosities;
} lightMaps;

uniform struct {
	float diffuse;
	float indirectDiffuse;
	float indirectSpecular;
} share;

void main()
{
	vec2 uv = gl_FragCoord.xy / lightMaps.resolution;
	vec3 indDiffuse = texture(lightMaps.indirectDiffuse, uv).rgb;
	vec3 indSpec = texture(lightMaps.indirectSpecular, uv).rgb;
	vec3 rad = texture(lightMaps.radiosities, uv).rgb;
	fColor = share.diffuse * rad + share.indirectDiffuse * indDiffuse + share.indirectSpecular * indSpec;
}