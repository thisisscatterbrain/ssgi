#version 410 core

#define M_PI 3.14159265358979323846


in vec3 vFrustumDirection;
layout(location = 0) out vec3 fSS1BRadiosity;

uniform struct {
	vec2 resolution;
	sampler2D linearDepths;
	sampler2D normals;
	sampler2D albedos;
	sampler2D positions;
} gBuffer;

uniform struct {
	sampler2D radiositiesTex;
} radiosityBuffer; 

uniform struct {
	float radius;
	int sampleCount;
} radiosityCfg;

uniform struct {
	float near;
	float far;
	vec3 position;
	float tanfovy2;
	float aspect;
	mat4 viewInv;
} camera;


//-----------------------------------------------------------------------------
// PRNG
//-----------------------------------------------------------------------------

// State of the PRNG.
// See: http://www.reedbeta.com/blog/2013/01/12/quick-and-easy-gpu-random-numbers-in-d3d11/
// for details.
uint rngState;

uint RandXorshift()
{
	// Rand function (see link above).
	rngState ^= (rngState << 13);
	rngState ^= (rngState >> 17);
	rngState ^= (rngState << 5);
	return rngState;
}

uint WangHash(uint seed)
{
	// Hash used to seed the PRNG (see link above).
	seed = (seed ^ 61) ^ (seed >> 16);
	seed *= 9;
	seed = seed ^ (seed >> 4);
	seed *= 0x27d4eb2d;
	seed = seed ^ (seed >> 15);
	return seed;
}

float Rand()
{
	// Returns a pseudo random number in [0, 1).
	const uint mx = 4096;
	uint m = mx - 1;
	uint r = RandXorshift();
	return float(r & m) / float(mx);
}

void InitPRNG()
{
	const uint m = 7;
	uint w = uint(gBuffer.resolution.x);
	uint i = uint(floor(gl_FragCoord.x)) & m;
	uint j = uint(floor(gl_FragCoord.y)) & m;
	uint s = i + j * w;
	rngState = WangHash(s);
}


float GetRadiusTC(float distance) 
{
	return radiosityCfg.radius / (2.0 * distance * camera.tanfovy2 * camera.aspect);
}

vec3 NormalFromUV(vec2 uv)
{
	return texture(gBuffer.normals, uv).xyz * 2.0 - 1.0;
}

vec3 PositionVCFromUV(vec2 uv)
{
	vec3 p;
	p.x = (uv.x * 2.0 - 1.0) * camera.tanfovy2 * camera.aspect;
	p.y = (uv.y * 2.0 - 1.0) * camera.tanfovy2;
	p.z = -1.0;
	float linearDepth = texture(gBuffer.linearDepths, uv).r  * (camera.far - camera.near) + camera.near;
	return linearDepth * p;
}

vec3 PositionFromUV1(vec2 uv)
{
	return texture(gBuffer.positions, uv).rgb;
}

vec3 PositionFromUV2(vec2 uv)
{
	float linearDepth = texture(gBuffer.linearDepths, uv).r;
	float mZVC = linearDepth * (camera.far - camera.near) + camera.near;
	return camera.position + mZVC * vFrustumDirection;
}

vec3 PositionFromUV3(vec2 uv)
{
	return (camera.viewInv * vec4(PositionVCFromUV(uv), 1.0)).xyz;
}

vec3 PositionFromUV(vec2 uv)
{
	return PositionFromUV1(uv);
}

vec3 RadiosityFromUV(vec2 uv)
{
	return texture(radiosityBuffer.radiositiesTex, uv).rgb;
}

float GetSample(inout vec3 position, inout vec3 normal, inout vec3 radiosity, 
	vec2 uv, float radiusTC)
{
	float e0 = Rand();
	float e1 = 2.0 * M_PI * Rand();
	vec2 rd = vec2(cos(e1), sin(e1));
	vec2 uvr = uv + radiusTC * e0 * rd;

	float linearDepth = texture(gBuffer.linearDepths, uvr).r;

	if (linearDepth == 1.0)
		return 0.0;

	position = PositionFromUV(uvr);
	normal = NormalFromUV(uvr);
	radiosity = RadiosityFromUV(uvr);
	return 1.0;
}

vec3 Get1BRadiosity()
{
	vec2 uv = gl_FragCoord.xy / gBuffer.resolution;
	
	float dist = texture(gBuffer.linearDepths, uv).r;
	
	if (dist >= 1.0)
		discard;

	dist = dist * (camera.far - camera.near) + camera.near;
	float r = GetRadiusTC(dist);
	vec3 p = PositionFromUV(uv);
	vec3 n = NormalFromUV(uv);
	vec3 alb = texture(gBuffer.albedos, uv).rgb;
	vec3 L = vec3(0.0);

	float nValidSamples = 0.0;

	for (int i = 0; i < radiosityCfg.sampleCount; i++) {
		vec3 ps, ns, Ls;
		float m = GetSample(ps, ns, Ls, uv, r);
		nValidSamples += m;
		vec3 d = normalize(ps - p);
		float l = length(d);
		d = d / l;
		L += m  * max(0.0, dot(d, n));
	}

	return L / nValidSamples;
	//return ps;
}

void main()
{
	vec2 uv = gl_FragCoord.xy / gBuffer.resolution;
	InitPRNG();
	fSS1BRadiosity = Get1BRadiosity();
	//fSS1BRadiosity = PositionFromUV1(uv);
	//fSS1BRadiosity = vec3(length(PositionFromUV2(uv) - PositionFromUV3(uv)));
}