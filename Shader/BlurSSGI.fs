#version 410 core

layout(location = 0) out vec3 fIndirectDiffuse;

uniform struct {
	sampler2D linearDepths;
	sampler2D normals;
} gBuffer;

uniform struct {
	vec2 resolution;
	sampler2D indirectDiffuse;
} ssgi;

uniform struct {
	vec2 direction;
	float radius;
	float sigmaS;
	float sigmaR;
	float sigmaR2;
} blur;

vec3 GetNormalWS(vec2 uv)
{
	return texture(gBuffer.normals, uv).xyz * 2.0 - 1.0;
}

void main()
{
	vec2 uv = gl_FragCoord.xy / ssgi.resolution;
	float w = 0.0;
	float ld = texture(gBuffer.linearDepths, uv).r;
	vec3 n = GetNormalWS(uv);

	vec3 indDiff = vec3(0.0);
	float av = 0.0;

	for (float i = -blur.radius; i <= blur.radius; i += 1.0) {
		vec2 uvn = (gl_FragCoord.xy + i * blur.direction) / ssgi.resolution;
		float ldn = texture(gBuffer.linearDepths, uvn).r;
		float s = i * blur.sigmaS;
		float ws = exp(-(s * s));
		float r = (ld - ldn) * blur.sigmaR;
		float wr = exp(-(r * r));
		vec3 nn = GetNormalWS(uvn);
		float r2 = blur.sigmaR2 * (1.0 - max(dot(n, nn), 0.0));
		float wr2 = exp(-(r2 * r2));
		float w2 = ws * wr * wr2;
		indDiff += w2 * texture(ssgi.indirectDiffuse, uvn).xyz;
		w += w2;
	}

	fIndirectDiffuse = indDiff / w;
}