#version 410 core

#define M_PI 3.141592654

layout(location = 0) out vec3 fIndirectDiffuse;
layout(location = 1) out vec3 fIndirectSpecular;
layout(location = 2) out float fGlossyness;

uniform struct {
	vec2 resolution;
	sampler2D linearDepths;
	sampler2D normals;
	sampler2D albedos;
	sampler2D reflectivities;
} gBuffer;

uniform struct {
	sampler2D radiosities;
} radiosityBuffer;

uniform struct {
	float tanFovy2;
	float aspect;
	mat4 viewInv;
	mat4 perspective;
	mat3 viewNormal;
} camera;

uniform struct {
	float radius;
	int sampleCount;
	float aaoBeta;
	float aaoEps;
	float aaoK;
	float aaoSigma;

	float radAs;

	float ssrRadius;
	float ssrSmoothnessMin;
	float ssrSmoothnessMax;
	float ssrDistMin;
	float ssrDistMax;
} ssgiCfg;

float Rand();
void InitPRNG();

//-----------------------------------------------------------------------------
float GetRadiusTS(float zDist)
{
	return ssgiCfg.radius / (zDist * camera.tanFovy2);
}
//-----------------------------------------------------------------------------
vec3 GetPositionWSFromLinearDepth(vec2 uv, float linearDepth)
{
	vec4 pVS;
	pVS.x = (uv.x * 2.0 - 1.0) * camera.tanFovy2 * camera.aspect;
	pVS.y = (uv.y * 2.0 - 1.0) * camera.tanFovy2;
	pVS.z = -1.0;
	pVS *= linearDepth;
	pVS.w = 1.0;
	return (camera.viewInv * pVS).xyz;
}
//-----------------------------------------------------------------------------
vec3 GetPositionWS(vec2 uv)
{
	vec4 pVS;
	pVS.x = (uv.x * 2.0 - 1.0) * camera.tanFovy2 * camera.aspect;
	pVS.y = (uv.y * 2.0 - 1.0) * camera.tanFovy2;
	pVS.z = -1.0;
	pVS *= texture(gBuffer.linearDepths, uv).r;
	pVS.w = 1.0;
	return (camera.viewInv * pVS).xyz;
}
//-----------------------------------------------------------------------------
vec3 GetNormalWS(vec2 uv)
{
	return texture(gBuffer.normals, uv).xyz * 2.0 - 1.0;
}
//-----------------------------------------------------------------------------
float GetSample(inout vec3 position, inout vec3 normal, inout vec3 radiosity, 
	float radiusTS, vec2 uv)
{
	float phi = 2 * M_PI * Rand();
	vec2 dir = vec2(cos(phi), sin(phi));
	vec2 uvr = uv + Rand() * radiusTS * dir;

	float ld = texture(gBuffer.linearDepths, uvr).r;

	if (ld == 0.0)
		return 1.0;

	position = GetPositionWSFromLinearDepth(uvr, ld);
	normal = GetNormalWS(uvr);
	radiosity = texture(radiosityBuffer.radiosities, uvr).rgb;
	return 1.0;
}
//-----------------------------------------------------------------------------
void ComputeSSGI(inout float ambientVisibility, inout vec3 radiosity,
	vec3 position, vec3 normal, float ld, vec2 uv)
{
	InitPRNG();

	float r = GetRadiusTS(ld);
	float av = 0.0;
	vec3 rad = vec3(0.0);

	for (int i = 0; i < ssgiCfg.sampleCount; i++) {
		vec3 ps, ns, rs;
		float va = GetSample(ps, ns, rs, r, uv);
		vec3 v = ps - position;
		float dist = length(v);
		av += va * max(0.0, (dot(v, normal) - ld * ssgiCfg.aaoBeta)) / (dist *dist + ssgiCfg.aaoEps);
		v /= dist;
		rad += rs * max(0.0, dot(v, normal)) * max(0.0, dot(-v, ns));
	}

	ambientVisibility = max(0.0, 1.0 - ssgiCfg.aaoSigma / float(ssgiCfg.sampleCount) * av);
	radiosity = ssgiCfg.radAs * rad / float(ssgiCfg.sampleCount);
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// SCREEN SPACE REFLECTIONS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
vec3 GetNormalVS(vec2 uv)
{
	return camera.viewNormal * GetNormalWS(uv);
}
//-----------------------------------------------------------------------------
vec3 GetPositionVSFromLinearDepth(vec2 uv, float linearDepth)
{
	vec3 pVS;
	pVS.x = (uv.x * 2.0 - 1.0) * camera.tanFovy2 * camera.aspect;
	pVS.y = (uv.y * 2.0 - 1.0) * camera.tanFovy2;
	pVS.z = -1.0;
	pVS *= linearDepth;
	return pVS;
}
//-----------------------------------------------------------------------------
vec3 GetPositionVS(vec2 uv)
{
	float ld = texture(gBuffer.linearDepths, uv).r;
	return GetPositionVSFromLinearDepth(uv, ld);
}
//-----------------------------------------------------------------------------
vec3 GetDirectionFromTC(vec2 tc)
{
	vec3 r;
	r.x = (tc.x * 2.0 - 1.0) * camera.tanFovy2 * camera.aspect;
	r.y = (tc.y * 2.0 - 1.0) * camera.tanFovy2;
	r.z = -1.0;
	return r;
}
//-----------------------------------------------------------------------------
float GetIntersectionRayPlane(vec3 c, float dotON, vec3 n)
{
	return dotON / dot(c, n);
}
//-----------------------------------------------------------------------------
void ComputeSSR(inout vec3 specular, inout float glossyness, vec3 positionVS,
	 vec3 normalVS, vec2 uv)
{
	vec4 specRef = texture(gBuffer.reflectivities, uv);

	if (specRef.a == 0.0) {
		return;
	}

	vec3 v = normalize(-positionVS);
	vec3 r = 2.0 * dot(v, normalVS) * normalVS - v;
	vec3 e = positionVS + ssgiCfg.ssrRadius * r;
	vec3 n = cross(r, cross(r, v));
	float dotON = dot(n, positionVS);

	vec2 sTS = uv;
	vec4 eClip = camera.perspective * vec4(e, 1.0);
	vec2 eTS = eClip.xy / eClip.w * 0.5 + 0.5; 
	vec2 dTS = eTS - sTS;
	float lenTS = length(dTS);

	// Choose main axis to march along.
	vec2 texelSize = 1.0 / gBuffer.resolution;

	if (dTS.x * camera.aspect < dTS.y) {
		dTS /= dTS.y;
	} else {
		dTS /= dTS.x;
	}

	dTS *= (2.0 * texelSize);
	vec2 off = dTS;

	specular = vec3(0.0, 0.0, 0.0);
	glossyness = 0.0;

	// March the ray in screen space.
	while (length(off) < lenTS) {
		vec2 sPrev;
		vec2 s = uv + off;
		vec3 c = GetDirectionFromTC(s);
		float z = GetIntersectionRayPlane(c, dotON, n);
		float zPrev;
		float lds = texture(gBuffer.linearDepths, s).r;
		
		// If an intersection was found.
		if (z > lds && lds != 0.0) {

			// Make sure the the distance between the point on the ray and the 
			// the intersected geometry is reasonably small, otherwise this 
			// fragment would not be suited for SSR.
			if (!(z - lds < 0.05)) {
				return;
			}
		
			vec2 sInt = (sPrev * (z - lds) + s * (lds - zPrev)) / (z - zPrev);

			// Make sure the starting coordinate and the intersection 
			// coordinate are at least one pixel apart.
			if (abs(sInt.x - uv.x) < 2.0 * texelSize.x && 
				abs(sInt.y - uv.y) < 2.0 * texelSize.y) {
				return;
			}

			// Compute specular light.
			vec3 ps = GetPositionVS(sInt);
			vec3 ns = GetNormalVS(sInt);
			vec3 diff = ps - positionVS;
			float dist = length(diff);
			diff /= dist;
			dist = dist < 0.05 ? 0.05 : dist;
			float d1 = max(0.0, dot(normalVS, diff));
			float d2 = max(0.0, dot(ns, -diff));

			if (d1 * d2 == 0.0) {
				return;
			}

			//specular = texture(radiosityBuffer.radiosities, sInt).rgb * d1 * d2 * 9.0;
			specular = texture(radiosityBuffer.radiosities, sInt).rgb  * d1 ;

						
			float smoothness = specRef.w;

			// TODO: Fix the glossy exponent in the gbuffer reflectivity map!
			glossyness = clamp(1.0 / smoothness * dist * dist / 
				(ssgiCfg.ssrRadius * ssgiCfg.ssrRadius), 0.0, 1.0);
			return;
		}

		off += dTS;
		sPrev = s;
		zPrev = z;
	}
}
//-----------------------------------------------------------------------------
void main()
{
	vec2 uv = gl_FragCoord.xy / gBuffer.resolution;
	vec3 rad;
	float ssao;

	float ld = texture(gBuffer.linearDepths, uv).r;

	if (ld == 0.0)
		discard;

	vec3 p = GetPositionWSFromLinearDepth(uv, ld);
	vec3 n = GetNormalWS(uv);

	ComputeSSGI(ssao, rad, p, n, ld, uv);
	vec3 alb = texture(gBuffer.albedos, uv).rgb;
	fIndirectDiffuse = alb * (ssao * vec3(1.0) + rad);

	vec3 pVS = GetPositionVSFromLinearDepth(uv, ld);
	vec3 nVS = GetNormalVS(uv);

	float glossyness;
	vec3 spec;
	ComputeSSR(spec, glossyness, pVS, nVS, uv);
	fIndirectSpecular = spec;
	fGlossyness = glossyness;
}
//-----------------------------------------------------------------------------