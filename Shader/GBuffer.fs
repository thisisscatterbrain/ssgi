#version 410 core

in vec3 vNormalWC;
in float vZVC;

layout(location = 0) out float fLinearDepth;
layout(location = 1) out vec3 fNormal;
layout(location = 2) out vec4 fAlbedo;
layout(location = 3) out vec4 fReflectivity;

uniform struct {
	vec3 albedo;
	vec3 reflectivity;
	float cosinePower;
} material;

void main()
{
	fLinearDepth = -vZVC;
	fNormal = vNormalWC * 0.5 + 0.5;
	fAlbedo = vec4(material.albedo, 1.0);
	fReflectivity = vec4(material.reflectivity, material.cosinePower);
}