#version 410 core

layout(location = 0) in vec2 position;

out vec3 vDirectionWS;

uniform struct {
	vec3 position;
	mat4 perspectiveViewInv;
} camera; 

void main()
{
	vec4 pWS = camera.perspectiveViewInv * vec4(position, -1.0, 1.0);
	vec3 dir = pWS.xyz / pWS.w - camera.position;
	vDirectionWS = dir / abs(dir.z);
	gl_Position = vec4(position, 0.0, 1.0);
}