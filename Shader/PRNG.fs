#version 410 core

//-----------------------------------------------------------------------------
// PRNG
//-----------------------------------------------------------------------------

// State of the PRNG.
// See: http://www.reedbeta.com/blog/2013/01/12/quick-and-easy-gpu-random-numbers-in-d3d11/
// for details.
uint rngState;

uint RandXorshift()
{
	// Rand function (see link above).
	rngState ^= (rngState << 13);
	rngState ^= (rngState >> 17);
	rngState ^= (rngState << 5);
	return rngState;
}

uint WangHash(uint seed)
{
	// Hash used to seed the PRNG (see link above).
	seed = (seed ^ 61) ^ (seed >> 16);
	seed *= 9;
	seed = seed ^ (seed >> 4);
	seed *= 0x27d4eb2d;
	seed = seed ^ (seed >> 15);
	return seed;
}

float Rand()
{
	// Returns a pseudo random number in [0, 1).
	const uint mx = 4096;
	uint m = mx - 1;
	uint r = RandXorshift();
	return float(r & m) / float(mx);
}

void InitPRNG()
{
	const uint m = 7;
	uint w = 1280;
	uint i = uint(floor(gl_FragCoord.x)) & m;
	uint j = uint(floor(gl_FragCoord.y)) & m;
	uint s = i + j * w;
	rngState = WangHash(s);
}
