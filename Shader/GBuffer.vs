#version 410 core

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
out vec3 vNormalWC;
out float vZVC;

uniform struct {
	mat4 view;
	mat4 perspective;
} camera;

void main()
{
	vec4 pVC = camera.view * vec4(position, 1.0);
	vZVC = pVC.z;
	vNormalWC = normal;
	gl_Position = camera.perspective * pVC;
}