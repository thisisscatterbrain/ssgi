#version 410 core

#define M_PI 3.141592654

layout(location = 0) out vec3 fIndirectSpecular;
//layout(location = 1) out float fGlossyness;

uniform struct {
	vec2 resolution;
	sampler2D linearDepths;
	sampler2D normals;
	sampler2D albedos;
	sampler2D reflectivities;
} gBuffer;

uniform struct {
	sampler2D radiosities;
	sampler2D indirectDiffuse;
} lightFields;

uniform struct {
	float tanFovy2;
	float aspect;
	mat4 perspective;
	mat3 viewNormal;
} camera;

uniform struct {
	float ssrRadius;
	float ssrDistMin;
	float ssrDistMax;
} ssgiCfg;

//-----------------------------------------------------------------------------
// SCREEN SPACE REFLECTIONS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
vec3 GetNormalWS(vec2 uv)
{
	return texture(gBuffer.normals, uv).xyz * 2.0 - 1.0;
}
//-----------------------------------------------------------------------------
vec3 GetNormalVS(vec2 uv)
{
	return camera.viewNormal * GetNormalWS(uv);
}
//-----------------------------------------------------------------------------
vec3 GetPositionVSFromLinearDepth(vec2 uv, float linearDepth)
{
	vec3 pVS;
	pVS.x = (uv.x * 2.0 - 1.0) * camera.tanFovy2 * camera.aspect;
	pVS.y = (uv.y * 2.0 - 1.0) * camera.tanFovy2;
	pVS.z = -1.0;
	pVS *= linearDepth;
	return pVS;
}
//-----------------------------------------------------------------------------
vec3 GetPositionVS(vec2 uv)
{
	float ld = texture(gBuffer.linearDepths, uv).r;
	return GetPositionVSFromLinearDepth(uv, ld);
}
//-----------------------------------------------------------------------------
vec3 GetDirectionFromTC(vec2 tc)
{
	vec3 r;
	r.x = (tc.x * 2.0 - 1.0) * camera.tanFovy2 * camera.aspect;
	r.y = (tc.y * 2.0 - 1.0) * camera.tanFovy2;
	r.z = -1.0;
	return r;
}
//-----------------------------------------------------------------------------
float GetIntersectionRayPlane(vec3 c, float dotON, vec3 n)
{
	return dotON / dot(c, n);
}
//-----------------------------------------------------------------------------
void ComputeSSR(inout vec3 specular, inout float glossyness, vec3 positionVS,
	vec3 normalVS, vec2 uv)
{
	vec4 specRef = texture(gBuffer.reflectivities, uv);

	if (specRef.a == 0.0) {
		return;
	}

	vec3 v = normalize(-positionVS);
	vec3 r = 2.0 * dot(v, normalVS) * normalVS - v;
	vec3 e = positionVS + ssgiCfg.ssrRadius * r;
	vec3 n = cross(r, cross(r, v));
	float dotON = dot(n, positionVS);

	vec2 sTS = uv;
	vec4 eClip = camera.perspective * vec4(e, 1.0);
	vec2 eTS = eClip.xy / eClip.w * 0.5 + 0.5;
	vec2 dTS = eTS - sTS;
	float lenTS = length(dTS);

	// Choose main axis to march along.
	vec2 texelSize = 1.0 / gBuffer.resolution;

	if (dTS.x * camera.aspect < dTS.y) {
		dTS /= dTS.y;
	}
	else {
		dTS /= dTS.x;
	}

	dTS *= (2.0 * texelSize);
	vec2 off = dTS;

	specular = vec3(0.0, 0.0, 0.0);
	glossyness = 0.0;

	// March the ray in screen space.
	while (length(off) < lenTS) {
		vec2 sPrev;
		vec2 s = uv + off;
		vec3 c = GetDirectionFromTC(s);
		float z = GetIntersectionRayPlane(c, dotON, n);
		float zPrev;
		float lds = texture(gBuffer.linearDepths, s).r;
		vec3 ps = GetPositionVS(s);

		// If an intersection was found.
//		if (length(c * z - ps) <= 0.015) {
		if (z > lds && lds != 0.0) {

			vec2 sInt = (sPrev * (z - lds) + s * (lds - zPrev)) / (z - zPrev);

			// Make sure the starting coordinate and the intersection 
			// coordinate are at least one pixel apart.
			//if (abs(sInt.x - uv.x) < 1.0 * texelSize.x &&
			//	abs(sInt.y - uv.y) < 1.0 * texelSize.y) {
			//	return;
			//}

			float llds = texture(gBuffer.linearDepths, sInt).r;

			// Make sure the the distance between the point on the ray and the 
			// the intersected geometry is reasonably small, otherwise this 
			// fragment would not be suited for SSR.
			if (!(z - llds < 0.05)) {
				return;
			}

			// Compute specular light.
			ps = GetPositionVS(sInt);
			vec3 ns = GetNormalVS(sInt);
			vec3 diff = ps - positionVS;
			float dist = length(diff);

			if (dist < 0.05)
				return;

			diff /= dist;
			float d1 = max(0.0, dot(normalVS, diff));


			//specular = texture(radiosityBuffer.radiosities, sInt).rgb * d1 * d2 * 9.0;
			specular = (texture(lightFields.indirectDiffuse, sInt).rgb) * d1;
			//specular = (texture(lightFields.indirectDiffuse, sInt).rgb);
			//specular = vec3(0.0, 1.0, 0.0);

			float smoothness = specRef.w;

			// TODO: Fix the glossy exponent in the gbuffer reflectivity map!
			glossyness = clamp(1.0 / smoothness * dist * dist /
				(ssgiCfg.ssrRadius * ssgiCfg.ssrRadius), 0.0, 1.0);
			return;
		}

		off += dTS;
		sPrev = s;
		zPrev = z;
	}
}
//-----------------------------------------------------------------------------
void main()
{
	vec2 uv = gl_FragCoord.xy / gBuffer.resolution;

	float ld = texture(gBuffer.linearDepths, uv).r;

	if (ld == 0.0)
		discard;

	vec3 pVS = GetPositionVSFromLinearDepth(uv, ld);
	vec3 nVS = GetNormalVS(uv);

	float glossyness;
	vec3 spec;
	ComputeSSR(spec, glossyness, pVS, nVS, uv);
	fIndirectSpecular = spec;
//	fGlossyness = glossyness;
}
//-----------------------------------------------------------------------------