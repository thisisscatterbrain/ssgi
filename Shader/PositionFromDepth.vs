#version 410 core

layout(location = 0) in vec2 position;
layout(location = 1) in vec3 frustumDirection;

out vec3 vFrustumDirection;

void main()
{
	vFrustumDirection = frustumDirection;
	gl_Position = vec4(position, 0.0, 1.0);
}
