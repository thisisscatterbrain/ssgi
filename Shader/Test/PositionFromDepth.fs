#version 410 core

in vec3 vFrustumDirection;
layout(location = 0) out vec3 fPosition;

uniform struct {
	vec2 resolution;
	sampler2D linearDepth;
	sampler2D positions;
	sampler2D normals;
} gBuffer;

uniform struct {
	vec3 position;
	float near;
	float far;
} camera;

void InitPRNG();
float Rand();


vec3 GetPosition1(vec2 uv)
{
	return texture(gBuffer.positions, uv).xyz;
}

vec3 GetPosition2(vec2 uv)
{
	float d = texture(gBuffer.linearDepth, uv).r * (camera.far - camera.near) + camera.near;
	return camera.position + d * vFrustumDirection;
}

vec3 GetPosition(vec2 uv)
{
	return GetPosition2(uv);
}

vec3 GetNormal(vec3 pos)
{
	vec3 dx = dFdx(pos);
	vec3 dy = dFdy(pos);
	return normalize(cross(dx, dy));
}

vec3 GetSamplePosition(vec2 uv)
{
	const float r = 0.05;
	vec2 uvr = uv + r;
	return GetPosition(uvr);
}

void main()
{
	InitPRNG();
	vec2 uv = gl_FragCoord.xy / gBuffer.resolution;
	vec3 p = GetPosition(uv);
	vec3 ps = GetSamplePosition(uv);
	vec3 n = GetNormal(p);
	fPosition = vec3(dot(normalize(p - ps), n));
//	fPosition = GetPosition1(uv) - GetPosition2(uv);
//	fPosition = GetPosition1(uv);
}